CREATE DATABASE [GosicoSTD] COLLATE Cyrillic_General_CI_AS
GO

USE [GosicoSTD]
GO

CREATE TABLE [dbo].[AcademicGroup](
  [idAcademicGroup] [int] IDENTITY(1,1) NOT NULL,
  [nameAGroup] [varchar](100) NOT NULL,
  [descAGroup] [varchar](255) NULL,
  [idChair] [int] NOT NULL,
  [isTeachers] [bit] NULL,
  CONSTRAINT [PK_AcademicGroup] PRIMARY KEY CLUSTERED
    (
      [idAcademicGroup] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[AcademicTerm](
  [idAcademicTerm] [int] IDENTITY(1,1) NOT NULL,
  [nameATerm] [varchar](45) NOT NULL,
  [descATerm] [varchar](255) NULL,
  CONSTRAINT [PK_AcademicTerm] PRIMARY KEY CLUSTERED
    (
      [idAcademicTerm] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[AccessNote](
  [idAccessNote] [int] NOT NULL,
  [nameAccessNote] [varchar](20) NOT NULL,
  [descAccessNote] [varchar](255) NULL,
  CONSTRAINT [PK_AccessNote] PRIMARY KEY CLUSTERED
    (
      [idAccessNote] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Attendance](
  [idAttendance] [int] IDENTITY(1,1) NOT NULL,
  [userAttendance] [varchar](100) NULL,
  [descAttendance] [varchar](255) NULL,
  [idUserRole] [int] NOT NULL,
  [idSubject] [int] NOT NULL,
  [date] [date] NOT NULL,
  CONSTRAINT [PK_Attendance] PRIMARY KEY CLUSTERED
    (
      [idAttendance] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Chair](
  [idChair] [int] IDENTITY(1,1) NOT NULL,
  [nameChair] [varchar](255) NOT NULL,
  [descChair] [varchar](255) NULL,
  [idFaculty] [int] NOT NULL,
  CONSTRAINT [PK_Chair] PRIMARY KEY CLUSTERED
    (
      [idChair] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Faculty](
  [idFaculty] [int] IDENTITY(1,1) NOT NULL,
  [nameFaculty] [varchar](255) NOT NULL,
  [descFaculty] [varchar](255) NULL,
  [idInstitute] [int] NOT NULL,
  CONSTRAINT [PK_Faculty] PRIMARY KEY CLUSTERED
    (
      [idFaculty] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Institute](
  [idInstitute] [int] IDENTITY(1,1) NOT NULL,
  [nameInstitute] [varchar](255) NOT NULL,
  [descInstitute] [varchar](255) NULL,
  CONSTRAINT [PK_Institute] PRIMARY KEY CLUSTERED
    (
      [idInstitute] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Mark](
  [idMark] [int] IDENTITY(1,1) NOT NULL,
  [nameMark] [varchar](45) NOT NULL,
  [descMark] [varchar](255) NULL,
  [date] [date] NOT NULL,
  [idSubject] [int] NOT NULL,
  [idUserRole] [int] NOT NULL,
  CONSTRAINT [PK_Marks] PRIMARY KEY CLUSTERED
    (
      [idMark] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Note](
  [idNote] [int] IDENTITY(1,1) NOT NULL,
  [dateNote] [datetime] NOT NULL,
  [themeNote] [varchar](100) NOT NULL,
  [textNote] [varchar](max) NULL,
  [priority] [int] NULL,
  [typeNote] [int] NOT NULL,
  [accessNote] [int] NOT NULL,
  [ownerNote] [int] NOT NULL,
  [pathRS] [varchar](100) NULL,
  [dateEnd] [datetime] NULL,
  [room] [varchar](10) NULL,
  [noteGroup] [varchar](255) NULL,
  [idSubject] [int] not null default(0)
  CONSTRAINT [PK_Note] PRIMARY KEY CLUSTERED
    (
      [idNote] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[Priority](
  [idPriority] [int] NOT NULL,
  [namePriority] [varchar](20) NULL,
  [descPriority] [varchar](255) NULL,
  CONSTRAINT [PK_Priority] PRIMARY KEY CLUSTERED
    (
      [idPriority] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Subject](
  [idSubject] [int] IDENTITY(1,1) NOT NULL,
  [nameSubject] [varchar](100) NOT NULL,
  [descSubject] [varchar](255) NULL,
  [idAcademicTerm] [int] NOT NULL,
  [idChair] [int] NULL,
  CONSTRAINT [PK_Subject] PRIMARY KEY CLUSTERED
    (
      [idSubject] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SubjectGroupSpecification](
  [idSubjectGroupSpecification] [int] IDENTITY(1,1) NOT NULL,
  [idAcademicGroup] [int] NULL,
  [idSubject] [int] NULL,
  FOREIGN KEY (IdSubject) REFERENCES Subject,
  FOREIGN KEY (IdAcademicGroup) REFERENCES AcademicGroup,
  CONSTRAINT [PK_SubjectGroupSpecification] PRIMARY KEY CLUSTERED
    (
      [idSubjectGroupSpecification] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SubjectSpecification](
  [idSubjectSpecification] [int] IDENTITY(1,1) NOT NULL,
  [idUserRole] [int] NULL,
  [idSubject] [int] NULL,
  CONSTRAINT [PK_SubjectSpecification] PRIMARY KEY CLUSTERED
    (
      [idSubjectSpecification] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[TypeNote](
  [idTypeNote] [int] NOT NULL,
  [nameTypeNote] [varchar](20) NOT NULL,
  [descTypeNote] [varchar](255) NULL,
  CONSTRAINT [PK_typeNote] PRIMARY KEY CLUSTERED
    (
      [idTypeNote] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[User](
  [idUser] [int] IDENTITY(1,1) NOT NULL,
  [email] [varchar](100) NOT NULL,
  [password] [varchar](100) NOT NULL,
  [login] [varchar](100) NOT NULL,
  [name] [varchar](45) NOT NULL,
  [surname] [varchar](45) NOT NULL,
  [patronicName] [varchar](45) NULL,
  [dateOfBirth] [date] NULL,
  [telephone] [varchar](20) NULL,
  [sex] [bit] NULL,
  [activity] [bit] NULL,
  CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED
    (
      [idUser] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[UserGroup](
  [idUserGroup] [int] NOT NULL,
  [nameUserGroup] [varchar](20) NOT NULL,
  [descUserGroup] [varchar](255) NULL,
  CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED
    (
      [idUserGroup] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[UserGroupSpecification](
  [idUserGroupSpecification] [int] IDENTITY(1,1) NOT NULL,
  [idUser] [int] NOT NULL,
  [idUserGroup] [int] NOT NULL,
  CONSTRAINT [PK_UserGroupSpecification] PRIMARY KEY CLUSTERED
    (
      [idUserGroupSpecification] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[UserRole](
  [idUserRole] [int] IDENTITY(1,1) NOT NULL,
  [name] [varchar](45) NULL,
  [surname] [varchar](45) NULL,
  [patronicName] [varchar](45) NULL,
  [idUser] [int] NULL,
  CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED
    (
      [idUserRole] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[UserSpecification](
  [idUserSpecification] [int] IDENTITY(1,1) NOT NULL,
  [idUserRole] [int] NOT NULL,
  [idAcademicGroup] [int] NOT NULL,
  CONSTRAINT [PK_UserSpecification] PRIMARY KEY CLUSTERED
    (
      [idUserSpecification] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[AcademicGroup]  WITH CHECK ADD  CONSTRAINT [FK_AcademicGroup_Chair] FOREIGN KEY([idChair])
REFERENCES [dbo].[Chair] ([idChair])
GO
ALTER TABLE [dbo].[AcademicGroup] CHECK CONSTRAINT [FK_AcademicGroup_Chair]
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_SubjectSpecification] FOREIGN KEY([idSubject])
REFERENCES [dbo].[Subject] ([idSubject])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_SubjectSpecification]
GO
ALTER TABLE [dbo].[Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Attendance_User] FOREIGN KEY([idUserRole])
REFERENCES [dbo].[UserRole] ([idUserRole])
GO
ALTER TABLE [dbo].[Attendance] CHECK CONSTRAINT [FK_Attendance_User]
GO
ALTER TABLE [dbo].[Chair]  WITH CHECK ADD  CONSTRAINT [FK_Chair_Faculty] FOREIGN KEY([idFaculty])
REFERENCES [dbo].[Faculty] ([idFaculty])
GO
ALTER TABLE [dbo].[Chair] CHECK CONSTRAINT [FK_Chair_Faculty]
GO
ALTER TABLE [dbo].[Faculty]  WITH CHECK ADD  CONSTRAINT [FK_Faculty_Institute] FOREIGN KEY([idInstitute])
REFERENCES [dbo].[Institute] ([idInstitute])
GO
ALTER TABLE [dbo].[Faculty] CHECK CONSTRAINT [FK_Faculty_Institute]
GO
ALTER TABLE [dbo].[Mark]  WITH CHECK ADD  CONSTRAINT [FK_Marks_SubjectSpecification] FOREIGN KEY([idSubject])
REFERENCES [dbo].[Subject] ([idSubject])
GO
ALTER TABLE [dbo].[Mark] CHECK CONSTRAINT [FK_Marks_SubjectSpecification]
GO
ALTER TABLE [dbo].[Mark]  WITH CHECK ADD  CONSTRAINT [FK_Marks_User] FOREIGN KEY([idUserRole])
REFERENCES [dbo].[UserRole] ([idUserRole])
GO
ALTER TABLE [dbo].[Mark] CHECK CONSTRAINT [FK_Marks_User]
GO
ALTER TABLE [dbo].[Note]  WITH CHECK ADD  CONSTRAINT [FK_Note_AccessNote] FOREIGN KEY([accessNote])
REFERENCES [dbo].[AccessNote] ([idAccessNote])
GO
ALTER TABLE [dbo].[Note] CHECK CONSTRAINT [FK_Note_AccessNote]
GO
ALTER TABLE [dbo].[Note]  WITH CHECK ADD  CONSTRAINT [FK_Note_Priority] FOREIGN KEY([priority])
REFERENCES [dbo].[Priority] ([idPriority])
GO
ALTER TABLE [dbo].[Note] CHECK CONSTRAINT [FK_Note_Priority]
GO
ALTER TABLE [dbo].[Note]  WITH CHECK ADD  CONSTRAINT [FK_Note_typeNote] FOREIGN KEY([typeNote])
REFERENCES [dbo].[TypeNote] ([idTypeNote])
GO
ALTER TABLE [dbo].[Note] CHECK CONSTRAINT [FK_Note_typeNote]
GO
ALTER TABLE [dbo].[Note]  WITH CHECK ADD  CONSTRAINT [FK_Note_User] FOREIGN KEY([ownerNote])
REFERENCES [dbo].[User] ([idUser])
GO
ALTER TABLE [dbo].[Note] CHECK CONSTRAINT [FK_Note_User]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [FK_Subject_AcademicTerm] FOREIGN KEY([idAcademicTerm])
REFERENCES [dbo].[AcademicTerm] ([idAcademicTerm])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [FK_Subject_AcademicTerm]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [FK_Subject_Chair] FOREIGN KEY([idChair])
REFERENCES [dbo].[Chair] ([idChair])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [FK_Subject_Chair]
GO
ALTER TABLE [dbo].[SubjectSpecification]  WITH CHECK ADD  CONSTRAINT [FK_SubjectSpecification_Subject] FOREIGN KEY([idSubject])
REFERENCES [dbo].[Subject] ([idSubject])
GO
ALTER TABLE [dbo].[SubjectSpecification] CHECK CONSTRAINT [FK_SubjectSpecification_Subject]
GO
ALTER TABLE [dbo].[SubjectSpecification]  WITH CHECK ADD  CONSTRAINT [FK_SubjectSpecification_User] FOREIGN KEY([idUserRole])
REFERENCES [dbo].[UserRole] ([idUserRole])
GO
ALTER TABLE [dbo].[SubjectSpecification] CHECK CONSTRAINT [FK_SubjectSpecification_User]
GO
ALTER TABLE [dbo].[UserGroupSpecification]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupSpecification_User] FOREIGN KEY([idUser])
REFERENCES [dbo].[User] ([idUser])
GO
ALTER TABLE [dbo].[UserGroupSpecification] CHECK CONSTRAINT [FK_UserGroupSpecification_User]
GO
ALTER TABLE [dbo].[UserGroupSpecification]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupSpecification_UserGroup] FOREIGN KEY([idUserGroup])
REFERENCES [dbo].[UserGroup] ([idUserGroup])
GO
ALTER TABLE [dbo].[UserGroupSpecification] CHECK CONSTRAINT [FK_UserGroupSpecification_UserGroup]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([idUser])
REFERENCES [dbo].[User] ([idUser])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
ALTER TABLE [dbo].[UserSpecification]  WITH CHECK ADD  CONSTRAINT [FK_UserSpecification_AcademicGroup] FOREIGN KEY([idAcademicGroup])
REFERENCES [dbo].[AcademicGroup] ([idAcademicGroup])
GO
ALTER TABLE [dbo].[UserSpecification] CHECK CONSTRAINT [FK_UserSpecification_AcademicGroup]
GO
ALTER TABLE [dbo].[UserSpecification]  WITH CHECK ADD  CONSTRAINT [FK_UserSpecification_UserRole] FOREIGN KEY([idUserRole])
REFERENCES [dbo].[UserRole] ([idUserRole])
GO
ALTER TABLE [dbo].[UserSpecification] CHECK CONSTRAINT [FK_UserSpecification_UserRole]
GO
USE [master]
GO
ALTER DATABASE [GosicoSTD] SET  READ_WRITE
GO
