insert into AccessNote
(idAccessNote, nameAccessNote, descAccessNote)
  select 1, 'Личный', null
  UNION all
  select 2, 'Групповой', null
  UNION all
  select 3, 'Общий', null

insert into Priority
(idPriority, namePriority, descPriority)
  select 1, 'Высокий', null
  UNION all
  select 2, 'Средний', null
  UNION all
  select 3, 'Низкий', null

insert into TypeNote
(idTypeNote, nameTypeNote, descTypeNote)
  select 1, 'Занятие', null
  UNION all
  select 2, 'Встреча', null
  UNION all
  select 3, 'Праздник', null
  UNION all
  select 4, 'Справочные материалы', null
  UNION all
  select 5, 'Другое', null

insert into UserGroup
(idUserGroup ,nameUserGroup, descUserGroup)
  select 1,'Admin', null
  UNION all
  select 2,'Teacher', null
  UNION all
  select 3,'Student', null
  UNION all
  select 4,'Captain', null

insert into [User]
(
  email, password, login, name, surname, activity
)
values
  (
    'admin@mail.ru', '81dc9bdb52d04dc20036dbd8313ed055', 'Admin', 'Системный', 'Администратор', 1
  )

insert into UserGroupSpecification
(idUser, idUserGroup)
VALUES
  (
    1, 1
  )

set IDENTITY_INSERT subject on

insert into Subject (
  idSubject,
  nameSubject,
  descSubject,
  idAcademicTerm,
  idChair)
values(0, 'null', 'null', null, null)