package Calendar.controllers;

import Notes.models.DAONote;
import Notes.models.EventData;
import core.Helpers.ValidationString;
import core.models.DAOUser;
import core.models.NoteModel;
import core.models.UsersModel;
import org.apache.http.annotation.ThreadSafe;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by homax on 12.02.17.
 */
@RequestMapping("/apps/Calendar/event")
@Controller
public class CalendarEventController {

    @Resource(name = "DAONote")
    DAONote daoNote;

    @Resource(name = "DAOUser")
    DAOUser daoUser;

    // TODO Нужно найти алтернативное решение.
    @Async
    @RequestMapping(method = RequestMethod.POST)
    public synchronized @ResponseBody List<EventData> getEvents(
            @RequestParam(value = "start") @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
            @RequestParam(value = "end", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end,
            @RequestParam(value = "_", required = false) String param,
            @RequestParam(value = "priority", required = false) int priority,
            @RequestParam(value = "type", required = false) int type,
            HttpSession session)
    {
        ValidationString validationString = new ValidationString();

        List<EventData> response = new ArrayList<EventData>();
        ArrayList<NoteModel> notes;

        UsersModel user = (UsersModel) session.getAttribute("currentUser");
        int id = user.getIdUser();

        int idUserRole = (int)session.getAttribute("currentRole");

        notes = (ArrayList<NoteModel>) daoNote
                .getAllNotesByUserId(
                        id,
                        idUserRole,
                        priority,
                        type,
                        start,
                        end,
                        validationString);

        if(validationString.isSucces)
        {
            NoteModel note = null;
            for(int i = 0; i < notes.size(); i++) {
                 note = (NoteModel)notes.get(i);

                String title = note.getThemeNote();
                Date dateStart = note.getDateNote();
                Date dateEnd = note.getDateEnd();
                int idNote = note.getIdNote();

                EventData event = new EventData();
                event.setTitle(title);
                event.setStart(dateStart);
                event.setEnd(dateEnd);
                event.setIdNote(idNote);

                response.add(event);
            }
        }

        return response;

    }

}
