package Calendar.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * Created by homax on 02.11.16.
 */
@RequestMapping("/apps/Calendar")
@Controller
public class CalendarIndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model uiModel, HttpSession session)
    {
        return "modules/Calendar/views/calendarPanel";
    }

}
