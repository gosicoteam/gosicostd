package EJ.controllers;

import EJ.models.DAOEj;
import EJ.models.User;
import EJ.models.Value;
import core.Helpers.ValidationString;
import core.models.AcademicGroup;
import core.models.Subject;
import core.models.UsersModel;
import core.responses.UserResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by homax on 08.11.16.
 */
@RequestMapping("/apps/EJ")
@Controller
public class EJIndexController {

    @Resource(name = "DAOEj")
    DAOEj daoEj;

    @RequestMapping(value = "/saveMarks", method = RequestMethod.POST)
    public @ResponseBody
    UserResponse saveMarksByIdUser()
    {
        UserResponse response = new UserResponse();



        return response;
    }

    @RequestMapping(value = "/setGroup", method = RequestMethod.POST)
    public @ResponseBody
    UserResponse setGroupId(HttpSession session, @RequestParam("idAcademicGroup") int idAcademicGroup)
    {
        UserResponse response = new UserResponse();

        session.setAttribute("idAcademicGroup", idAcademicGroup);

        return response;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model uiModel, HttpSession session,
                        @RequestParam(value = "idSubject", required = false) Integer idSubject) {

        ValidationString validationString = new ValidationString();
        int idUserRole = (int)session.getAttribute("currentRole");
        Integer idAcademicGroup = (Integer)session.getAttribute("idAcademicGroup");
        idAcademicGroup = idAcademicGroup == null ? 0 : idAcademicGroup;

        // Список групп преподаваемых
        List<AcademicGroup> groups = daoEj.getGroupsByTeacherRole(idUserRole, validationString);
        if(validationString.isSucces){
            uiModel.addAttribute("groups", groups);
            uiModel.addAttribute("selectedGroup", idAcademicGroup);
        }

        // Добавление список предметов
        List<Subject> subjectList = daoEj.getSubjectOfCurrentUserRole(idUserRole, idAcademicGroup ,validationString);//

        if(validationString.isSucces) {
            uiModel.addAttribute("subjectList", subjectList);
        }

        if(idSubject == null)
        {
            if(subjectList == null) {
                idSubject = 0;
            }
            else {
                if(subjectList.size() != 0) {
                    idSubject = new Integer(subjectList.get(0).getIdSubject());
                }
                else {
                    idSubject = 0;
                }
            }
        }

        // Добавление дат
        List<java.sql.Timestamp> dateList = daoEj.getAllDates(idSubject, validationString);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        List<String> dates = new ArrayList<String>();

        for(java.sql.Timestamp date : dateList)
        {
            String rez = formatter.format(date.getTime());
            dates.add(rez);
        }

        if(validationString.isSucces) {
            uiModel.addAttribute("dateList", dates);
        }

//        List<User> userList = new ArrayList<>();

//        User newUser = new User();
//        newUser.setNumber(1);
//        newUser.setUserID(0);
//        newUser.setUserName("Иванов Иван Иванович");
//        List<Value> values = new ArrayList<>();
//
//        Value value = new Value();
//        value.setDate("2017-01-01");
//        value.setValue("+");
//        values.add(value);
//        newUser.setValues(values);
//
//        userList.add(newUser);
        List<User> userList = daoEj.getAllStudents(
                idAcademicGroup,
                idSubject,
                dateList,
                validationString);

        uiModel.addAttribute("userList", userList);





        return "modules/EJ/views/ejPanel";
    }
}
