package EJ.models;

import core.Helpers.ValidationHelper;
import core.Helpers.ValidationString;
import core.models.AbstractDAO;
import core.models.AcademicGroup;
import core.models.Subject;
import org.springframework.stereotype.Service;

import org.hibernate.Session;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by homax on 14.05.17.
 */
@Service("DAOEj")
public class DAOEj extends AbstractDAO {

    public List<User> getAllStudents(int idAcademicGroup, int idSubject, List<Timestamp> dateList ,ValidationString validationString)
    {
        List<User> users = null;

        List<Object[]> objUsers = null;
        List<Object[]> objValues = null;

        Session session = this.getSession();
        try{
            tran = session.beginTransaction();

            objUsers = session.createSQLQuery("select ur.idUserRole, (ur.surname +  ' ' + ur.name) from UserRole ur\n" +
                     "join UserSpecification us on ur.idUserRole = us.idUserRole\n" +
                     "join AcademicGroup ag on ag.idAcademicGroup = us.idAcademicGroup\n" +
                     "where ag.idAcademicGroup = :idAcademicGroup")
                     .setInteger("idAcademicGroup", idAcademicGroup)
                     .list();

            users = new ArrayList<User>(objUsers.size());

            objValues = session.createSQLQuery("select a.idUserRole, a.date, a.userAttendance from UserSpecification us\n" +
                    "join AcademicGroup ag on ag.idAcademicGroup = us.idAcademicGroup and ag.idAcademicGroup = 1\n" +
                    "left join Attendance a on us.idUserRole = a.idUserRole\n" +
                    "where\n" +
                    "  a.idSubject = :idSubject\n" +
                    "  and  ag.idAcademicGroup = :idAcademicGroup " +
                    " and a.userAttendance is not null")
                    .setInteger("idAcademicGroup", idAcademicGroup)
                    .setInteger("idSubject", idSubject)
                    .list();

            int counter = 1;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            for(Object[] userObj : objUsers) {
                User user = new User();
                user.setUserID((int)userObj[0]);
                user.setUserName((String)userObj[1]);
                user.setNumber(counter);

                List<Value> values = new ArrayList<Value>(dateList.size());
                for(Timestamp date : dateList)
                {
                    Value value = new Value();
                    value.setDate(formatter.format(date));
                    value.setValue("");
                    values.add(value);
                }

                for(Object[] valueObj : objValues){
                    int userID = (int)valueObj[0];
                    if(userID == user.getUserID())
                    {
                        for(Value value : values) {
                            if(value.getDate().equals(valueObj[1].toString())) {
                                value.setValue((String)valueObj[2]);
                            }
                        }
                    }
                }

                user.setValues(values);
                users.add(user);
                counter++;
            }

            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }


        return users;
    }

    public List<Timestamp> getAllDates(int idSubject, ValidationString validationString)
    {
        List<Timestamp> dateslist = null;
        Session session = this.getSession();
        try{
            tran = session.beginTransaction();

            dateslist = (List<Timestamp>) session
                    .createSQLQuery("select dateNote from Note where idSubject = :idSubject and typeNote = 1")
                    .setInteger("idSubject", idSubject)
                    .list();

            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        return dateslist;
    }

    public List<AcademicGroup> getGroupsByTeacherRole(int idUserRole, ValidationString validationString)
    {
        List<AcademicGroup> groupList = null;
        Session session = this.getSession();
        try{
            tran = session.beginTransaction();

            groupList = (List<AcademicGroup>) session
                    .createSQLQuery("select ag.* from AcademicGroup ag\n" +
                            "join SubjectGroupSpecification sgs on sgs.idAcademicGroup = ag.idAcademicGroup\n" +
                            "join SubjectSpecification ss on sgs.idSubject = ss.idSubject\n" +
                            "where ss.idUserRole = :idUserRole")
                    .addEntity(AcademicGroup.class)
                    .setInteger("idUserRole", idUserRole)
                    .list();

            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        return groupList;
    }

    public List<Subject> getSubjectOfCurrentUserRole(int idUserRole, int idAcademicGroup ,ValidationString validationString)
    {
        List<Subject> subjectList = null;
        Session session = this.getSession();
        try{
            tran = session.beginTransaction();

            subjectList = (List<Subject>) session
                    .createSQLQuery("select s.* from Subject s\n" +
                            "join SubjectSpecification ss on s.idSubject = ss.idSubject\n\n" +
                            "join SubjectGroupSpecification sgs on sgs.idSubject = s.idSubject\n" +
                            "where ss.idUserRole = :idUserRole and sgs.idAcademicGroup = :idAcademicGroup")
                    .addEntity(Subject.class)
                    .setInteger("idUserRole", idUserRole)
                    .setInteger("idAcademicGroup", idAcademicGroup)
                    .list();

            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        return subjectList;
    }

}
