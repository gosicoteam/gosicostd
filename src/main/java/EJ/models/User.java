package EJ.models;

import java.util.List;

/**
 * Created by homax on 04.05.17.
 */
public class User {
    private int userID;

    private int number;

    private String userName;

    private List<Value> values;

    public void setNumber(int number) {
        this.number = number;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    public int getNumber() {
        return number;
    }

    public int getUserID() {
        return userID;
    }

    public List<Value> getValues() {
        return values;
    }

    public String getUserName() {
        return userName;
    }
}
