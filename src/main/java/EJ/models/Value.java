package EJ.models;

import java.util.Date;

/**
 * Created by homax on 05.05.17.
 */
public class Value {

    private String value;

    private String date;

    public void setDate(String date) {
        this.date = date;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public String getValue() {
        return value;
    }
}
