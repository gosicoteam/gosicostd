package Messages.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by homax on 08.11.16.
 */
@RequestMapping("/apps/Messages")
@Controller
public class MessagesIndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String index()
    {

        return "modules/Messages/views/messagesPanel";
    }

}
