package Notes.controllers;

import Notes.models.DAONote;
import Notes.models.ResponseNoteCreate;
import core.Helpers.ValidationString;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by homax on 15.05.17.
 */
@RequestMapping("/note/delete")
@Controller
public class NoteDeleteActionController {

    @Resource(name = "DAONote")
    DAONote daoNote;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseNoteCreate deleteNoteById(@RequestParam("idNote") int idNote)
    {
        ResponseNoteCreate response = new ResponseNoteCreate();
        ValidationString validationString = new ValidationString();

        daoNote.deleteNote(idNote, validationString);

        if (validationString.isSucces) {
            response.setCode("200");
            response.setMessage("SUCCESS");
        } else {
            response.setCode("500");
            response.setMessage("ERROR");
        }

        return response;

    }
}
