package Notes.controllers;

import Notes.models.DAONote;
import core.Helpers.ValidationString;
import core.models.NoteModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * Created by homax on 15.02.17.
 */
@RequestMapping("/getevent")
@Controller
public class NoteGetActionController {

    @Resource(name = "DAONote")
    DAONote daoNote;

    @RequestMapping(method = RequestMethod.GET)
    public String getEvent(@RequestParam(value = "id") int id, HttpSession session)
    {
        ValidationString validationString = new ValidationString();

        NoteModel note = daoNote.getNoteById(id, validationString);

        if(validationString.isSucces)
        {
            session.setAttribute("note", note);

            return "/modules/Notes/views/showNote";
        }

        return null;
    }

}
