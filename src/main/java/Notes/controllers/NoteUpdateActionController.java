package Notes.controllers;

import Notes.models.DAONote;
import Notes.models.EventData;
import Notes.models.ResponseNoteCreate;
import core.Helpers.ValidationString;
import core.models.NoteModel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by homax on 17.02.17.
 */
@RequestMapping("/update/note")
@Controller
public class NoteUpdateActionController {

    @Resource(name = "DAONote")
    DAONote daoNote;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseNoteCreate updateNoteByDrag(@RequestBody EventData event) {
        ValidationString validationString = new ValidationString();
        ResponseNoteCreate response = null;

        int id = event.getIdNote();
        Date start = event.getStart();
        Date end = event.getEnd();

        daoNote.updateNoteById(id, start, end, validationString);

        if (validationString.isSucces) {
            response.setCode("200");
            response.setMessage("SUCCESS");
        } else {
            response.setCode("500");
            response.setMessage("ERROR");
        }

        return response;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public @ResponseBody ResponseNoteCreate updateNote(
            @RequestParam int id,
            @RequestParam String theme,
            @RequestParam String text
    )
    {
        ResponseNoteCreate response = new ResponseNoteCreate();
        ValidationString validationString = new ValidationString();

        NoteModel note = daoNote.getNoteById(id, validationString);

        note.setTextNote(text);
        note.setThemeNote(theme);

        daoNote.updateNote(note, validationString);

        if (validationString.isSucces) {
            response.setCode("200");
            response.setMessage("SUCCESS");
        } else {
            response.setCode("500");
            response.setMessage("ERROR");
        }

        return response;
    }

}
