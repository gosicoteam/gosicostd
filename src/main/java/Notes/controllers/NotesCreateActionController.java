package Notes.controllers;

import Notes.models.ResponseNoteCreate;
import Notes.models.DAONote;
import core.Helpers.ValidationString;
import core.models.NoteModel;
import core.models.Subject;
import core.models.UsersModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by homax on 13.02.17.
 */
@RequestMapping("/apps/Notes/create")
@Controller
public class NotesCreateActionController {

    @Resource(name = "DAONote")
    DAONote daoNote;

    @RequestMapping(method = RequestMethod.GET)
    public String page(Model uiModel, HttpSession session) {

        UsersModel user = (UsersModel) session.getAttribute("currentUser");
        ValidationString validationString = new ValidationString();

        List<Subject> subjects = daoNote.getAllTeacherSubjects(user.getIdUser(), validationString);

        if(validationString.isSucces)
        {
            uiModel.addAttribute("subjects", subjects);
        }

        return "modules/Notes/views/createNote";
    }

    @RequestMapping(method = RequestMethod.POST) public @ResponseBody
    ResponseNoteCreate createNote(@RequestBody NoteModel note, HttpSession session) {
        ResponseNoteCreate response = new ResponseNoteCreate();
        ValidationString validationString = new ValidationString();

        UsersModel user = (UsersModel) session.getAttribute("currentUser");
        note.setOwnerNote(user.getIdUser());

        daoNote.createNewNote(note, validationString);

        if (validationString.isSucces) {
            response.setCode("200");
            response.setMessage("Success");
        } else {
            response.setCode("500");
            response.setMessage("Error");
        }

        return response;
    }

}
