package Notes.models;

import core.Helpers.ValidationHelper;
import core.Helpers.ValidationString;
import core.models.AbstractDAO;
import core.models.NoteModel;
import core.models.Subject;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by homax on 12.02.17.
 */
@Service("DAONote")
public class DAONote extends AbstractDAO {

    public void deleteNote(int idNote, ValidationString validationString){

        Session session = this.getSession();
        try{
            tran = session.beginTransaction();
            session
                    .createSQLQuery("delete from Note where idNote = :idNote")
                    .setInteger("idNote", idNote)
                    .executeUpdate();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

    }

    public List<Subject> getAllTeacherSubjects(int idUser, ValidationString validationString){
        List<Subject> subjects = new ArrayList<>();
        Session session = this.getSession();
        try{
            tran = session.beginTransaction();

            subjects = (List<Subject>)session
                    .createSQLQuery("select s.* from [User] u\n" +
                            "join UserRole ur on u.idUser = ur.idUser\n" +
                            "join UserSpecification us on us.idUserRole = ur.idUserRole\n" +
                            "join AcademicGroup ag on ag.idAcademicGroup = us.idAcademicGroup\n" +
                            "join SubjectSpecification sspec on sspec.idUserRole = ur.idUserRole\n" +
                            "join Subject s on s.idSubject = sspec.idSubject\n" +
                            "where ag.isTeachers = 1 and u.idUser = :idUser")
                    .addEntity(Subject.class)
                    .setInteger("idUser", idUser)
                    .list();

            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        return subjects;
    }

    public List<NoteModel> getAllNotesByUserId(int idUser, int idUserRole,  int idPriority, int type ,Date start, Date end, ValidationString validationString) {
        List<NoteModel> notes = null;
        Session session = this.getSession();
        try {
            tran = session.beginTransaction();
            if(type == 0) {
                notes = session
                        .createSQLQuery("SELECT * FROM Note WHERE ownerNote = :idUser AND (dateNote >= :startDate OR dateEnd <= :endDate) " +
                                "AND priority = :idPriority AND typeNote != 1")
                        .addEntity(NoteModel.class)
                        .setParameter("idUser", idUser)
                        .setDate("startDate", start)
                        .setDate("endDate", end)
                        .setParameter("idPriority", idPriority)
                        .list();
            }
            else {

                if(type == 1) {
                    notes = session
                            .createSQLQuery("SELECT n.* FROM Note n join SubjectGroupSpecification sgs on sgs.idSubject = n.idSubject\n" +
                                    "join AcademicGroup ag on sgs.idAcademicGroup = ag.idAcademicGroup\n" +
                                    "join UserSpecification us on us.idAcademicGroup = ag.idAcademicGroup\n" +
                                    "join UserRole ur on ur.idUserRole = us.idUserRole " +
                                    "WHERE ur.idUserRole = :idUserRole AND (dateNote >= :startDate OR dateEnd <= :endDate) " +
                                    "AND priority = :idPriority AND typeNote = :typeValue")
                            .addEntity(NoteModel.class)
                            .setParameter("idUserRole", idUserRole)
                            .setDate("startDate", start)
                            .setDate("endDate", end)
                            .setParameter("idPriority", idPriority)
                            .setParameter("typeValue", type)
                            .list();
                }
                else {
                    notes = session
                            .createSQLQuery("SELECT * FROM Note WHERE ownerNote = :idUser AND (dateNote >= :startDate OR dateEnd <= :endDate) " +
                                    "AND priority = :idPriority AND typeNote = :typeValue")
                            .addEntity(NoteModel.class)
                            .setParameter("idUser", idUser)
                            .setDate("startDate", start)
                            .setDate("endDate", end)
                            .setParameter("idPriority", idPriority)
                            .setParameter("typeValue", type)
                            .list();
                }
            }


            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        return notes;

    }

    public void createNewNote(NoteModel note, ValidationString validationString) {
        Session session = this.getSession();
        try {
            tran = session.beginTransaction();
            session.save(note);
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }
    }

    public void updateNoteById(int id, Date start, Date end, ValidationString validationString)
    {
        Session session = this.getSession();
        Transaction tran = null;

        try{
            tran = session.beginTransaction();
            session
                    .createSQLQuery("UPDATE Note Set dateNote = :startDate, dateEnd = :startEnd WHERE idNote = :idNote ; ")
                    .setTimestamp("startDate", new java.sql.Date(start.getTime()))
                    .setTimestamp("startEnd", new java.sql.Date(end.getTime()))
                    .setInteger("idNote", id)
                    .executeUpdate();
            tran.commit();
        }catch (Exception ex){
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }
    }

    public List<NoteModel> getNotes(String searchParam)
    {
        List<NoteModel> noteModels = new ArrayList<>();

        Session session = this.getSession();
        try{
            tran = session.beginTransaction();
            noteModels = (List<NoteModel>) session
                    .createSQLQuery("SELECT * FROM Note WHERE themeNote LIKE '%' + :searchParam + '%'")
                    .addEntity(NoteModel.class)
                    .setParameter("searchParam", searchParam)
                    .list();
        } catch (Exception ex) {
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        return  noteModels;
    }

    public NoteModel getNoteById(int id, ValidationString validationString)
    {
        NoteModel note = null;
        Session session = this.getSession();
        try{
            tran = session.beginTransaction();
            note = (NoteModel) session.get(NoteModel.class, id);
            tran.commit();
        }catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }

        return note;
    }

    public void updateNote(NoteModel note ,ValidationString validationString) {

        Session session = this.getSession();
        try{
            tran = session.beginTransaction();
            session.update(note);
            tran.commit();
        } catch(Exception exception) {
            ValidationHelper.ParsingOfException(exception, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

    }
}
