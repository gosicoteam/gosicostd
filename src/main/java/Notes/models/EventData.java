package Notes.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by homax on 12.02.17.
 */
public class EventData implements Serializable{

    String title;

    Date start;

    Date end;

    int idNote;

    public String getTitle() {
        return title;
    }

    public Date getEnd() {
        return end;
    }

    public Date getStart() {
        return start;
    }

    public int getIdNote() {
        return idNote;
    }

    public void setIdNote(int idNote) {
        this.idNote = idNote;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public void setStart(Date start) {
        this.start = start;
    }
}
