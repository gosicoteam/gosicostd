package Notes.models;

import java.io.Serializable;

/**
 * Created by homax on 13.02.17.
 */
public class ResponseNoteCreate implements Serializable {

    String message;

    String code;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
