package Phones.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by homax on 08.11.16.
 */
@RequestMapping("/apps/Phones")
@Controller
public class PhonesIndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String index()
    {

        return "modules/Phones/views/phonesPanel";
    }

}
