package ReferenceMaterials.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by homax on 08.11.16.
 */
@RequestMapping("/apps/ReferenceMaterials")
@Controller
public class ReferenceMaterialsIndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String index()
    {

        return "modules/ReferenceMaterials/views/referenceMaterialsPanel";
    }

}
