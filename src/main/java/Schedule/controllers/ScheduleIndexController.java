package Schedule.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by homax on 08.11.16.
 */
@RequestMapping("/apps/Schedule")
@Controller
public class ScheduleIndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String index()
    {

        return "modules/Schedule/views/schedulePanel";
    }

}
