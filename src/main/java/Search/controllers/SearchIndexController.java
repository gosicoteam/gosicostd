package Search.controllers;

import Notes.models.DAONote;
import core.models.NoteModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by homax on 08.11.16.
 */
@RequestMapping("/apps/Search")
@Controller
public class SearchIndexController {

    @Resource(name = "DAONote")
    DAONote daoNote;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model uiModel, @RequestParam(value = "searchParam", required=false) String searchParam)
    {
        List<NoteModel> list = daoNote.getNotes(searchParam == null ? "" : searchParam);

        uiModel.addAttribute("list", list);

        return "modules/Search/views/searchPanel";
    }

}
