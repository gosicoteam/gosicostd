package core.Helpers;

/**
 * Created by homax on 11.02.17.
 */
public final class ValidationHelper {

    private ValidationHelper(){}

    public static void ParsingOfException(Exception ex, ValidationString validationString) {
        String message = ex.getMessage();

        String stackTrace = "";
        StackTraceElement[] stackTraceElements = ex.getStackTrace();

        for (StackTraceElement elem : stackTraceElements) {
            stackTrace += elem.toString() + "\n";
        }

        validationString.isSucces = false;
        validationString.setMessage(message);
        validationString.setStackTrace(stackTrace);

    }

}
