package core.Helpers;

/**
 * Created by homax on 11.02.17.
 */
public class ValidationString {
    String message;

    String stackTrace;

    public boolean isSucces = true;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String toString()
    {
        return "Message " + message + "\n" + "StackTrace " + stackTrace;
    }
}
