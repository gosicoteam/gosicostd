package core.controllers;

import core.Helpers.ValidationString;
import core.models.DAOAdd;
import core.models.DAOGet;
import core.models.DAOUser;
import core.models.UsersModel;
import core.responses.RolesAndGroupsResponse;
import core.responses.SubjectResponse;
import core.responses.TeacherResponse;
import core.responses.UserResponse;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by homax on 10.02.17.
 */
@Controller
public class AdminController {

    @Resource(name = "DAOUser")
    DAOUser daoUser;

    @Resource(name = "DAOGet")
    DAOGet daoGet;

    @Resource(name = "DAOAdd")
    DAOAdd daoAdd;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String index(Model uiModel, @RequestParam(value = "searchParam", required=false) String searchParam)
    {
        ValidationString validationString = new ValidationString();
        List<UsersModel> users = daoUser.allUsers(validationString, searchParam);

        if(validationString.isSucces)
        {
            uiModel.addAttribute("users", users);
        }

        return "core/views/admin";
    }

    @Async
    @RequestMapping(value = "/get/chairs", method = RequestMethod.POST)
    public synchronized  @ResponseBody
    Map<Integer, String> getChairs()
    {
        Map<Integer, String> chairs;
        ValidationString validationString = new ValidationString();

        chairs = daoGet.getChairs(validationString);

        return chairs;
    }

    @Async
    @RequestMapping(value = "/get/groups", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public synchronized  @ResponseBody
    Map<Integer, String> getGroups(@RequestParam("idChair") String idChair)
    {
        Map<Integer, String> groups;
        ValidationString validationString = new ValidationString();

        groups = daoGet.getGroups(validationString, idChair);

        return groups;
    }

    @Async
    @RequestMapping(value = "/get/disciplines", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public synchronized  @ResponseBody
    List<SubjectResponse> getDisciplines(@RequestParam("idChair") String idChair)
    {
        List<String> disciplines;
        List<SubjectResponse> subjects = new ArrayList<>();
        ValidationString validationString = new ValidationString();

        disciplines = daoGet.getDisciplines(validationString, idChair);

        for(String disciplien : disciplines){
            SubjectResponse subject = new SubjectResponse();
            subject.setName(disciplien);
            subjects.add(subject);
        }

        return  subjects;
    }

    @Async
    @RequestMapping(value = "/get/teachers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public synchronized  @ResponseBody
    Map<Integer, String> getTeachers(@RequestParam("idChair") String idChair)
    {
        Map<Integer, String> teachers;
        ValidationString validationString = new ValidationString();

        teachers = daoGet.getTeachers(validationString, idChair);

        return teachers;
    }

    @Async
    @RequestMapping(value = "/get/teachers_groups", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public synchronized  @ResponseBody
    List<TeacherResponse> getTeachersGroups(@RequestParam("idChair") String idChair)
    {
        List<TeacherResponse> teachers;
        ValidationString validationString = new ValidationString();

        teachers = daoGet.getTeachersGroups(validationString, idChair);

        return teachers;
    }

    @Async
    @RequestMapping(value = "/get/student_and_groups", method = {RequestMethod.POST, RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
    public synchronized @ResponseBody
    List<RolesAndGroupsResponse> getStudentsAndGroups(@RequestParam(value = "idChair", required = false) String idChair)
    {
        List<RolesAndGroupsResponse> groups;
        ValidationString validationString = new ValidationString();

        groups = daoGet.getStudentsAndGroups(validationString, idChair);

        return groups;
    }

    @RequestMapping(value = "/user/disattach", method = RequestMethod.POST)
    public @ResponseBody
    UserResponse disattachRole(
            @RequestParam("idUser") int idUser,
            @RequestParam("idUserRole") int idUserRole
    )
    {
        ValidationString validationString = new ValidationString();
        UserResponse response = new UserResponse();

        daoUser.disattachUserRole(idUserRole, validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Роль привязана");
        }
        else
        {
            response.setCode("400");
            response.setMsg("Фейл " + validationString.getStackTrace());
        }

        return response;
    }

    @RequestMapping(value = "/user/attach", method = RequestMethod.POST)
    public @ResponseBody
    UserResponse attachRole(
            @RequestParam("idUser") int idUser,
            @RequestParam("idUserRole") int idUserRole
    )
    {
        ValidationString validationString = new ValidationString();
        UserResponse response = new UserResponse();

        daoUser.attachUserRole(idUserRole, idUser, validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Роль привязана");
        }
        else
        {
            response.setCode("400");
            response.setMsg("Фейл " + validationString.getStackTrace());
        }

        return response;
    }


    @RequestMapping(value = "/add/dicsipline", method = RequestMethod.POST)
    public @ResponseBody
    UserResponse addDicsipline(@RequestParam("name") String name,
                               @RequestParam("teachers") String teachers,
                               @RequestParam("groups") String groups,
                               @RequestParam("idChair") int idChair)
    {
        ValidationString validationString = new ValidationString();
        UserResponse response = new UserResponse();

        List<String> teachersList = new ArrayList<>();
        List<String> groupsList = new ArrayList<>();

        String[] array = teachers.split(";");
        teachersList = Arrays.asList(array);

        array = groups.split(";");
        groupsList = Arrays.asList(array);

        daoAdd.addDiscipline(idChair, name, teachersList, groupsList ,validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Создана дисциплина");
        }
        else
        {
            response.setCode("400");
            response.setMsg("Фейл " + validationString.getStackTrace());
        }

        return response;

    }

}
