package core.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by homax on 30.10.16.
 */

@RequestMapping("/")
@Controller
public class IndexController {

    @RequestMapping(method = RequestMethod.GET)
    public String GoToIndex()
    {
        return "core/views/index";
    }

    @RequestMapping(value = "/forgotpass", method = RequestMethod.GET)
    public String GoToForgotPathPAge() {return "core/views/forgot-Pass";}

}
