package core.controllers;

import core.Helpers.ValidationString;
import core.models.DAOUser;
import core.models.UserRole;
import core.models.UsersModel;
import core.responses.UserResponse;
import org.apache.http.HttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

/**
 * Created by homax on 04.11.16.
 */
@RequestMapping("/mainPanel")
@Controller
public class MainPanelController {

    @Resource(name = "DAOUser")
    DAOUser daoUser;

    ValidationString validationString;

    @PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_USER')")
    @RequestMapping(method = RequestMethod.GET)
    public String createMainPanel(Model uiModel, HttpSession session)
    {
        validationString = new ValidationString();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName(); //get logged in username

        UsersModel user = daoUser.findUserByLogin(login, validationString);
        String userName = user.getName() + " " + user.getSurname();

        if(validationString.isSucces)
        {
            Integer role = (Integer)session.getAttribute("currentRole");

            uiModel.addAttribute("currentRole", role);
            uiModel.addAttribute("userName", userName);
            uiModel.addAttribute("currentUser", user);
            session.setAttribute("currentUser", user);
        }

        return "core/views/mainPanel";
    }

    @RequestMapping(value = "/setRole", method=RequestMethod.POST)
    public @ResponseBody
    UserResponse setUserRoleInSession(Model uiModel, HttpSession session,
                                      @RequestParam("idUserRole") int idUserRole)
    {
        UserResponse response = new UserResponse();
        ValidationString validationString = new ValidationString();

        UsersModel user = (UsersModel)session.getAttribute("currentUser");
        Set<UserRole> roles = user.getRoles();

        if(roles.isEmpty())
        {
            session.setAttribute("currentRole", 0);
        } else {
            session.setAttribute("currentRole", idUserRole);
        }


        return response;

    }

}
