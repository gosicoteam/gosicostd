package core.controllers;

import core.Helpers.ValidationString;
import core.models.*;
import core.responses.UserResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by homax on 01.11.16.
 */
// Класс, который будет управлять логикой над пользователем
@RequestMapping("/users")
@Controller
public class UsersController {

    @Resource(name="DAOUser")
    private DAOUser daoUser;

    @Resource(name="DAOUserGroup")
    private DAOUserGroup daoUserGroup;

    @Resource(name="DAOUserRole")
    private DAOUserRole daoUserRole;

    @Resource(name="DAOAcademicGroup")
    private DAOAcademicGroup daoAcademicGroup;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public @ResponseBody UserResponse addDicipline()
    {
        UserResponse userResponse = new UserResponse();


        return  userResponse;
    }

    // TODO Дублирование кода с методом addRole
    @RequestMapping(value = "/addTeacher", method = RequestMethod.POST)
    public @ResponseBody UserResponse addTeacher(@RequestParam("name") String name,
                                                 @RequestParam("surname") String surname,
                                                 @RequestParam("patronicName") String  patronicName,
                                                 @RequestParam("idChair") int idChair)
    {
        ValidationString validationString = new ValidationString();
        UserResponse response = new UserResponse();
        UserRole teacher = new UserRole();
        teacher.setName(name);
        teacher.setPatronicName(patronicName);
        teacher.setSurname(surname);

        AcademicGroup group = daoAcademicGroup.findTeacherGroupOfCreate(idChair);
        Set<AcademicGroup> groups = new HashSet<AcademicGroup>();
        groups.add(group);
        teacher.setGroups(groups);

        daoUserRole.createNewUser(teacher, validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Создана роль");
        }
        else
        {
            response.setCode("400");
            response.setMsg("Роль не создана");
        }

        return response;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody UserResponse addRole(@RequestBody UserRole userRole)
    {
        UserResponse response = new UserResponse();
        ValidationString validationString = new ValidationString();

        if(userRole.getGroup() != 0) {
            AcademicGroup group = daoAcademicGroup.findGroupById(userRole.getGroup());
            Set<AcademicGroup> groups = new HashSet<AcademicGroup>();
            groups.add(group);
            userRole.setGroups(groups);
        }
        else
        {
            AcademicGroup group = daoAcademicGroup.createAcademicGroup(userRole.getNameGroup(), userRole.getIdChair());
            Set<AcademicGroup> groups = new HashSet<AcademicGroup>();
            groups.add(group);
            userRole.setGroups(groups);
        }

        daoUserRole.createNewUser(userRole, validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Создана роль");
        }
        else
        {
            response.setCode("400");
            response.setMsg("Роль не создана");
        }

        return response;
    }

    @RequestMapping(value = "/checkPassword", method = RequestMethod.POST)
    public @ResponseBody UserResponse checkPassword(@RequestParam("password") String password, HttpSession session)
    {
        UserResponse response = new UserResponse();

        UsersModel currentUser = (UsersModel) session.getAttribute("currentUser");
        String currentPassword = currentUser.getPassword();

        if(currentPassword.equals(password))
        {
            response.setCode("1");
            response.setMsg("Пароль совпадает");
        }
        else
        {
            response.setCode("0");
            response.setMsg("Пароль не совпадает");
        }

        return response;
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String updateForm(HttpSession session)
    {
        UsersModel currentUser = (UsersModel)session.getAttribute("currentUser");

        return "core/views/personalAccount";
    }

    @RequestMapping(value = "/form/update", method = RequestMethod.POST)
    public @ResponseBody UserResponse update(@RequestBody UsersModel requestUsersModel, HttpSession session)
    {
        UsersModel currentUser = (UsersModel)session.getAttribute("currentUser");
        UserResponse response = new UserResponse();
        ValidationString validationString = new ValidationString();

        currentUser.setSurname(requestUsersModel.getSurname());
        currentUser.setName(requestUsersModel.getName());
        currentUser.setPatronicName(requestUsersModel.getPatronicName());
        currentUser.setEmail(requestUsersModel.getEmail());
        currentUser.setPassword(requestUsersModel.getPassword());
        currentUser.setTelephone(requestUsersModel.getTelephone());

        daoUser.updateUser(currentUser, validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Success updating");
        }
        else
        {
            response.setCode("500");
            response.setMsg(validationString.getMessage());
        }

        return response;

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public @ResponseBody UserResponse update(@RequestBody UsersModel requestUsersModel)
    {
        ValidationString validationString = new ValidationString();
        UserResponse response = new UserResponse();

        int id = requestUsersModel.getIdUser();
        Boolean isActive = requestUsersModel.getActivity();
        daoUser.updateUserById(id, isActive, validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Success updating");
        }
        else
        {
            response.setCode("500");
            response.setMsg(validationString.getMessage());
        }

        return response;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public @ResponseBody UserResponse delete(@RequestBody UsersModel requestUsersModel) {
        ValidationString validationString = new ValidationString();
        UserResponse response = new UserResponse();
        int id = requestUsersModel.getIdUser();

        daoUser.deleteUserById(id, validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Success deleting");

        }
        else
        {
            response.setCode("500");
            response.setMsg(validationString.getMessage());
        }

        return response;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createForm()
    {

        return "core/views/registration";
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public @ResponseBody UserResponse create(@RequestBody UsersModel requestUsersModel)
    {
        ValidationString validationString = new ValidationString();
        UserResponse response = new UserResponse();

        UsersModel userModel = fillFields(requestUsersModel);

        daoUser.createNewUser(userModel, validationString);

        if(validationString.isSucces)
        {
            response.setCode("200");
            response.setMsg("Success creating");
        }
        else
        {
            response.setCode("500");
            response.setMsg(validationString.getMessage());
        }

        return response;
    }

    private UsersModel fillFields(UsersModel requestUsersModel)
    {
        String login = requestUsersModel.getLogin();
        String password = requestUsersModel.getPassword();
        String email = requestUsersModel.getEmail();
        String name = requestUsersModel.getName();
        String surname = requestUsersModel.getSurname();
        String patronicName = requestUsersModel.getPatronicName();
        String phone = requestUsersModel.getTelephone();
        Boolean sex = requestUsersModel.getSex();
        Date birthDate = requestUsersModel.getDateOfBirth();
        int groupId = requestUsersModel.getGroup();

        UsersModel userModel = new UsersModel();

        userModel.setLogin(login);
        userModel.setPassword(DigestUtils.md5Hex(password));
        userModel.setEmail(email);
        userModel.setName(name);
        userModel.setSurname(surname);
        userModel.setPatronicName(patronicName);
        userModel.setTelephone(phone);
        userModel.setSex(sex);
        userModel.setDateOfBirth(birthDate);

        UserGroupModel group = daoUserGroup.findGroupById(groupId);
        Set<UserGroupModel> groups = new HashSet<UserGroupModel>();
        groups.add(group);
        userModel.setGroups(groups);

        return userModel;
    }

}
