package core.models;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.annotation.Resource;
import java.io.Closeable;

/**
 * Created by homax on 12.02.17.
 */
// TODO Возможно требуется адаптер для использования контрукции try()
public abstract class AbstractDAO  {

    @Resource(name = "sessionFactory")
    protected SessionFactory sessionFactory;

    protected Transaction tran;

    public Session getSession() {
        Session session;
        if (sessionFactory.isClosed()) {
            session = sessionFactory.openSession();
        } else {
            session = sessionFactory.getCurrentSession();
        }
        return session;
    }

    protected void closeSession(Session session)
    {
        if(session.isOpen()) {
            session.close();
        }
    }
}
