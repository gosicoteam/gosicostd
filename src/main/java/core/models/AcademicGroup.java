package core.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by homax on 19.03.17.
 */
@Entity
@Table(name = "AcademicGroup")
public class AcademicGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idAcademicGroup")
    private int idAcameicGroup;

    @Column(name = "nameAGroup")
    private String nameAGroup;

    @Column(name = "descAGroup")
    private String descAGroup;

    @Column(name = "isTeachers" ,columnDefinition = "BIT")
    public boolean isTeachers;

    @Column(name = "idChair")
    public int idChair;

    @ManyToMany(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            mappedBy = "groups",
            targetEntity = UserRole.class,
            fetch = FetchType.EAGER
    )
    Set<UserRole> users = new HashSet<UserRole>(0);

    public void setUsers(Set<UserRole> users) {
        this.users = users;
    }

    public Set<UserRole> getUsers() {
        return users;
    }

    public void setDescAGroup(String descAGroup) {
        this.descAGroup = descAGroup;
    }

    public void setIdAcameicGroup(int idAcameicGroup) {
        this.idAcameicGroup = idAcameicGroup;
    }

    public void setNameAGroup(String nameAGroup) {
        this.nameAGroup = nameAGroup;
    }

    public void setIdChair(int idChair) {
        this.idChair = idChair;
    }

    public int getIdChair() {
        return idChair;
    }

    public void setTeachers(boolean teachers) {
        isTeachers = teachers;
    }

    public int getIdAcameicGroup() {
        return idAcameicGroup;
    }

    public String getDescAGroup() {
        return descAGroup;
    }

    public String getNameAGroup() {
        return nameAGroup;
    }
}
