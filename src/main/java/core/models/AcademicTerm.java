package core.models;

import javax.persistence.*;

/**
 * Created by homax on 25.03.17.
 */
@Entity
@Table(name = "AcademicTerm")
public class AcademicTerm {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idAcademicTerm")
    private int idAcademicTerm;

    @Column(name = "nameATerm")
    private String nameATerm;

    @Column(name = "descATerm")
    private String descATerm;

    public int getIdAcademicTerm() {
        return idAcademicTerm;
    }

    public String getDescATerm() {
        return descATerm;
    }

    public String getNameATerm() {
        return nameATerm;
    }

    public void setDescATerm(String descATerm) {
        this.descATerm = descATerm;
    }

    public void setIdAcademicTerm(int idAcademicTerm) {
        this.idAcademicTerm = idAcademicTerm;
    }

    public void setNameATerm(String nameATerm) {
        this.nameATerm = nameATerm;
    }
}
