package core.models;

import core.Helpers.ValidationString;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

/**
 * Created by homax on 19.03.17.
 */
@Service("DAOAcademicGroup")
public class DAOAcademicGroup extends AbstractDAO {

    public AcademicGroup createAcademicGroup(String name, int idChair)
    {
        Session session = this.getSession();

        AcademicGroup academicGroup = null;

        try {
            tran = session.beginTransaction();
            academicGroup = new AcademicGroup();
            academicGroup.setNameAGroup(name);
            academicGroup.setTeachers(false);
            academicGroup.setIdChair(idChair);
            session.save(academicGroup);
            tran.commit();
        } catch (Exception ex) {
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }

        return academicGroup;
    }

    public AcademicGroup findGroupById(int id) {
        Session session = this.getSession();

        AcademicGroup group = null;
        try {
            tran = session.beginTransaction();
            group = (AcademicGroup) session.get(AcademicGroup.class, id);
            tran.commit();
        } catch (Exception ex) {
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }

        return group;
    }

    public AcademicGroup findTeacherGroupOfCreate(int idChair) {
        Session session = this.getSession();

        AcademicGroup group = null;
        try {
            tran = session.beginTransaction();

            group = (AcademicGroup) session.createSQLQuery(
                    "SELECT top 1 ag.* FROM AcademicGroup ag " +
                            "JOIN Chair c ON ag.idChair = c.idChair " +
                            "WHERE c.idChair = :idChair and ag.isTeachers = 1")
                    .addEntity("ag", AcademicGroup.class)
                    .setParameter("idChair", idChair)
                    .uniqueResult();

            if(group == null)
            {
                 session.createSQLQuery("INSERT INTO AcademicGroup VALUES (:name, :desc, :idChair, :isTeachers)")
                        .setParameter("name", "Преподаватели")
                        .setParameter("desc", "Группа преподавателей кафедры")
                        .setParameter("idChair", idChair)
                        .setParameter("isTeachers", 1)
                        .executeUpdate();

                group = (AcademicGroup) session.createSQLQuery(
                        "SELECT top 1 ag.* FROM AcademicGroup ag " +
                                "JOIN Chair c ON ag.idChair = c.idChair " +
                                "WHERE c.idChair = :idChair and ag.isTeachers = 1")
                        .addEntity("ag", AcademicGroup.class)
                        .setParameter("idChair", idChair)
                        .uniqueResult();
            }

            tran.commit();
        } catch (Exception ex) {
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }

        return group;
    }

}
