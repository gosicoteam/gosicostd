package core.models;

import com.microsoft.sqlserver.jdbc.SQLServerDataTable;
import core.Helpers.ValidationHelper;
import core.Helpers.ValidationString;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import java.sql.Types;
import java.util.List;

/**
 * Created by homax on 25.03.17.
 */
@Service("DAOAdd")
public class DAOAdd extends AbstractDAO {

    public void addDiscipline(int idChair,String name, List<String> teachers, List<String> groupsList, ValidationString validationString) {

        Session session = this.getSession();
        try {
            tran = session.beginTransaction();

            session
                    .createSQLQuery("INSERT INTO Subject\n" +
                            "(\n" +
                            "  nameSubject, idChair \n" +
                            ")\n" +
                            "VALUES\n" +
                            "(\n" +
                            "  :name, :idChair\n" +
                            "); "
                            )
                    .setParameter("name", name)
                    .setInteger("idChair", idChair)
                    .executeUpdate();

            int idSubject = (int) session
                    .createSQLQuery("SELECT MAX(idSubject) FROM Subject")
                    .uniqueResult();

            for(String id : teachers) {
                session
                        .createSQLQuery("INSERT INTO SubjectSpecification (idSubject, idUserRole) VALUES (:idSubject, :idUserRole)")
                        .setParameter("idSubject", idSubject)
                        .setParameter("idUserRole", id)
                        .executeUpdate();
            }

            for(String id : groupsList) {
                session
                        .createSQLQuery("INSERT INTO SubjectGroupSpecification (idSubject, idAcademicGroup) VALUES (:idSubject, :idAcademicGroup)")
                        .setParameter("idSubject", idSubject)
                        .setParameter("idAcademicGroup", id)
                        .executeUpdate();
            }

            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

    }


    private SQLServerDataTable createTableParam(String idSubject, List<String> teachers){

        SQLServerDataTable param = null;

        try {
            param = new SQLServerDataTable();

            param.addColumnMetadata("idSubject", Types.INTEGER);
            param.addColumnMetadata("idUserRole", Types.INTEGER);

            for(String teacher : teachers) {
                param.addRow(idSubject, teacher);
            }
        }
        catch(Exception ex)
        {

        }
        return param;
    }

}
