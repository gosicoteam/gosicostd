package core.models;

import core.Helpers.ValidationHelper;
import core.Helpers.ValidationString;
import core.responses.RolesAndGroupsResponse;
import core.responses.TeacherResponse;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by homax on 19.03.17.
 */
@Service("DAOGet")
public class DAOGet extends AbstractDAO {

    public Map<Integer, String> getChairs(ValidationString validationString) {
        Session session = this.getSession();
        List<Object[]> chairsObj = null;
        try {
            tran = session.beginTransaction();
            chairsObj = session
                    .createSQLQuery("SELECT * from ( SELECT c.idChair ,(i.nameInstitute + ', ' + f.nameFaculty + ', ' + c.nameChair) as Name FROM Institute i\n" +
                            "JOIN Faculty f on f.idInstitute = i.idInstitute\n" +
                            "JOIN Chair c on c.idFaculty = f.idFaculty) t " +
                            "order by Name")
                    .list();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        Map<Integer, String> chairs = new HashMap<>(chairsObj.size());

        for(Object[] item : chairsObj) {
            Integer id = (Integer)item[0];
            String name = (String)item[1];
            chairs.put(id, name);
        }

        return chairs;

    }

    public Map<Integer, String> getGroups(ValidationString validationString, String idChair) {
        Session session = this.getSession();
        List<Object[]> groupsObj = null;
        try {
            tran = session.beginTransaction();
            groupsObj = session
                    .createSQLQuery("select idAcademicGroup, nameAGroup from AcademicGroup where idChair = :idChair")
                    .setParameter("idChair", idChair)
                    .list();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        Map<Integer, String> groups = new HashMap<>(groupsObj.size());

        for(Object[] item : groupsObj) {
            Integer id = (Integer)item[0];
            String name = (String)item[1];
            groups.put(id, name);
        }

        return groups;

    }

    public List<TeacherResponse> getTeachersGroups(ValidationString validationString, String idChair) {
        Session session = this.getSession();
        List<Object[]> groupsObj = null;
        try {
            tran = session.beginTransaction();
            groupsObj = session
                    .createSQLQuery("select " +
                            "(surname + ' ' + substring(name, 1, 1) + '. ' + substring(patronicName,1,1) + '.')," +
                            " 1 as param from UserRole u\n" +
                            "join UserSpecification us on us.idUserRole = u.idUserRole\n" +
                            "join AcademicGroup ag on ag.idAcademicGroup = us.idAcademicGroup\n" +
                            "where ag.idChair = :idChair and ag.isTeachers = 1")
                    .setParameter("idChair", idChair)
                    .list();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        List<TeacherResponse> teachers = new ArrayList<>();

        for(Object[] item : groupsObj) {
            String name = (String)item[0];

            TeacherResponse teacher = new TeacherResponse();
            teacher.setName(name);
            teachers.add(teacher);
        }

        return teachers;

    }

    public Map<Integer, String> getTeachers(ValidationString validationString, String idChair) {
        Session session = this.getSession();
        List<Object[]> groupsObj = null;
        try {
            tran = session.beginTransaction();
            groupsObj = session
                    .createSQLQuery("select u.idUserRole, (surname + ' ' + substring(name, 1, 1) + '. ' + substring(patronicName,1,1) + '.') from UserRole u\n" +
                            "join UserSpecification us on us.idUserRole = u.idUserRole\n" +
                            "join AcademicGroup ag on ag.idAcademicGroup = us.idAcademicGroup\n" +
                            "where ag.idChair = :idChair and ag.isTeachers = 1")
                    .setParameter("idChair", idChair)
                    .list();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        Map<Integer, String> teachers = new HashMap<>(groupsObj.size());

        for(Object[] item : groupsObj) {
            Integer id = (Integer)item[0];
            String name = (String)item[1];
            teachers.put(id, name);
        }

        return teachers;

    }

    public List<String> getDisciplines(ValidationString validationString, String idChair)
    {
        Session session = this.getSession();
        List<String> disciplines = null;
        try {
            tran = session.beginTransaction();
            disciplines = session
                    .createSQLQuery("SELECT nameSubject FROM Subject s " +
                            "WHERE idChair = :idChair")
                    .setParameter("idChair", idChair)
                    .list();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        return disciplines;

    }

    public List<RolesAndGroupsResponse> getStudentsAndGroups(ValidationString validationString, String idChair) {
        Session session = this.getSession();
        List<Object[]> groupsObj = null;
        try {
            tran = session.beginTransaction();
            if(idChair == null || idChair.equals("")) {
                groupsObj = session
                        .createSQLQuery("SELECT  ur.idUserRole ,ag.nameAGroup ,(ur.surname + ' ' + ur.name) as Name FROM UserRole ur\n" +
                                "JOIN UserSpecification us on us.idUserRole = ur.idUserRole\n" +
                                "JOIN AcademicGroup ag on ag.idAcademicGroup = us.idAcademicGroup\n" +
                                "JOIN Chair c on c.idChair = ag.idChair\n")
                        .list();
            }
            else {
                groupsObj = session
                        .createSQLQuery("SELECT  ag.nameAGroup ,(ur.surname + ' ' + ur.name) as Name FROM UserRole ur\n" +
                                "JOIN UserSpecification us on us.idUserRole = ur.idUserRole\n" +
                                "JOIN AcademicGroup ag on ag.idAcademicGroup = us.idAcademicGroup\n" +
                                "JOIN Chair c on c.idChair = ag.idChair\n" +
                                "WHERE c.idChair = :idChair AND ag.IsTeachers = 0")
                        .setParameter("idChair", idChair)
                        .list();
            }
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }

        List<RolesAndGroupsResponse> groups = new ArrayList<RolesAndGroupsResponse>(groupsObj.size());

        for(Object[] item : groupsObj) {
            RolesAndGroupsResponse response = new RolesAndGroupsResponse();
            if(item.length > 2) {
                response.setIdUserRole(item[0].toString());
                response.setGroup(item[1].toString());
                response.setStudent(item[2].toString());
            }
            else{
                response.setGroup(item[0].toString());
                response.setStudent(item[1].toString());
            }

            groups.add(response);
        }

        return groups;

    }

}
