package core.models;

import core.Helpers.ModelHelper;
import core.Helpers.ValidationHelper;
import core.Helpers.ValidationString;
import org.hibernate.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by homax on 08.02.17.
 */
@Service("DAOUser")
public class DAOUser extends AbstractDAO {

    public void attachUserRole(int idUserRole, int idUser, ValidationString validationString)
    {
        Session session = this.getSession();

        try{
            tran = session.beginTransaction();
            session
                    .createSQLQuery("update UserRole set idUser = :idUser where idUserRole = :idUserRole")
                    .setInteger("idUser", idUser)
                    .setInteger("idUserRole", idUserRole)
                    .executeUpdate();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }

    }

    public void disattachUserRole(int idUserRole, ValidationString validationString)
    {
        Session session = this.getSession();

        try{
            tran = session.beginTransaction();
            session
                    .createSQLQuery("update UserRole set idUser = null where idUserRole = :idUserRole")
                    .setInteger("idUserRole", idUserRole)
                    .executeUpdate();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }

    }

    public UsersModel findUserByLogin(String login, ValidationString validationString) {
        Session session = this.getSession();
        UsersModel user = null;
        try {
            tran = session.beginTransaction();
            user = (UsersModel) session
                    .createSQLQuery("SELECT * FROM [User] WHERE login = :loginParam")
                    .addEntity(UsersModel.class)
                    .setParameter("loginParam", login)
                    .uniqueResult();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }

        return user;
    }

    public void updateUser(UsersModel user, ValidationString validationString)
    {
        Session session = this.getSession();

        try {
            tran = session.beginTransaction();
            session.update(user);
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }
    }

   public void createNewUser(UsersModel usersModel, ValidationString validationString) {

       Session session = this.getSession();

       try {
           tran = session.beginTransaction();
           session.save(usersModel);
           tran.commit();
       } catch (Exception ex) {
           ValidationHelper.ParsingOfException(ex, validationString);
           tran.rollback();
       } finally {
           this.closeSession(session);
       }
   }

    public List<UsersModel> allUsers(ValidationString validationString, String searchParam) {
        Session session = this.getSession();
        List<UsersModel> users;

        try {
            tran = session.beginTransaction();
            if(searchParam == null || searchParam == "") {
                users = (List<UsersModel>) session
                        .createSQLQuery("select * from [User]")
                        .addEntity(UsersModel.class)
                        .list();
            }
            else {
                users = (List<UsersModel>) session
                        .createSQLQuery("select * from [User] where login LIKE '%' + :searchParam + '%'")
                        .addEntity(UsersModel.class)
                        .setParameter("searchParam", searchParam)
                        .list();
            }
            tran.commit();
            return users;
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
            return null;
        }
        finally {
            this.closeSession(session);
        }
    }

    public void deleteUserById(int id, ValidationString validationString) {
        Session session = this.getSession();
        try {
            tran = session.beginTransaction();
            UsersModel user = new UsersModel();
            user.setIdUser(id);
            session.delete(user);
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }
    }


    public void updateUserById(int id, boolean isActive, ValidationString validationString) {
        Session session = this.getSession();

        try {
            tran = session.beginTransaction();
            Query query = session.createSQLQuery("UPDATE [User] set activity = :isActive where idUser = :id");
            query.setParameter("isActive", ModelHelper.ParseBooleanToBit(isActive));
            query.setParameter("id", id);
            query.executeUpdate();
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }
    }

}
