package core.models;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by homax on 09.02.17.
 */
@Service("DAOUserGroup")
public class DAOUserGroup extends AbstractDAO {

    public UserGroupModel findGroupById(int id) {
        Session session = this.getSession();

        UserGroupModel group = null;
        try {
            tran = session.beginTransaction();
            group = (UserGroupModel) session.get(UserGroupModel.class, id);
            tran.commit();
        } catch (Exception ex) {
            tran.rollback();
        }
        finally {
            this.closeSession(session);
        }

        return group;
    }

}
