package core.models;

import core.Helpers.ValidationHelper;
import core.Helpers.ValidationString;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

/**
 * Created by homax on 19.03.17.
 */
@Service("DAOUserRole")
public class DAOUserRole extends AbstractDAO {

    public void createNewUser(UserRole userRole, ValidationString validationString) {

        Session session = this.getSession();

        try {
            tran = session.beginTransaction();
            session.save(userRole);
            tran.commit();
        } catch (Exception ex) {
            ValidationHelper.ParsingOfException(ex, validationString);
            tran.rollback();
        } finally {
            this.closeSession(session);
        }
    }

}
