package core.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by homax on 12.02.17.
 */
@Entity
@Table(name = "Note")
public class NoteModel implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "idNote")
    int idNote;

    @Column(name = "dateNote")
    @JsonDeserialize
    Date dateNote;

    @Column(name = "themeNote")
    String themeNote;

    @Column(name = "textNote")
    String textNote;

    @Column(name = "priority")
    int priority;

    @Column(name = "typeNote")
    int typeNote;

    @Column(name = "accessNote")
    int accessNote;

    @Column(name = "ownerNote")
    int ownerNote;

    @Column(name = "pathRS")
    String pathRS;

    @Column(name ="dateEnd")
    @JsonDeserialize
    Date dateEnd;

    @Column(name = "room")
    String room;

    @Column(name = "noteGroup")
    String noteGroup;

    @Column(name = "idSubject", nullable = false)
    int idSubject;

    public int getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(int idSubject) {
        this.idSubject = idSubject;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public Date getDateNote() {
        return dateNote;
    }

    public int getAccessNote() {
        return accessNote;
    }

    public int getIdNote() {
        return idNote;
    }

    public int getOwnerNote() {
        return ownerNote;
    }

    public int getPriority() {
        return priority;
    }

    public int getTypeNote() {
        return typeNote;
    }

    public String getNoteGroup() {
        return noteGroup;
    }

    public String getPathRS() {
        return pathRS;
    }

    public String getRoom() {
        return room;
    }

    public String getTextNote() {
        return textNote;
    }

    public String getThemeNote() {
        return themeNote;
    }

    public void setAccessNote(int accessNote) {
        this.accessNote = accessNote;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public void setDateNote(Date dateNote) {
        this.dateNote = dateNote;
    }

    public void setIdNote(int idNote) {
        this.idNote = idNote;
    }

    public void setNoteGroup(String noteGroup) {
        this.noteGroup = noteGroup;
    }

    public void setOwnerNote(int ownerNote) {
        this.ownerNote = ownerNote;
    }

    public void setPathRS(String pathRS) {
        this.pathRS = pathRS;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setTextNote(String textNote) {
        this.textNote = textNote;
    }

    public void setThemeNote(String themeNote) {
        this.themeNote = themeNote;
    }

    public void setTypeNote(int typeNote) {
        this.typeNote = typeNote;
    }
}
