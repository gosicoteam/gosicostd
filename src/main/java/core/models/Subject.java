package core.models;

import javax.annotation.Generated;
import javax.persistence.*;

/**
 * Created by homax on 25.03.17.
 */
@Entity
@Table(name = "Subject")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="idSubject")
    private int idSubject;

    @Column(name = "nameSubject")
    private String nameSubject;

    @Column(name = "descSubject")
    private String descSubject;

    public int getIdSubject() {
        return idSubject;
    }

    public String getDescSubject() {
        return descSubject;
    }

    public String getNameSubject() {
        return nameSubject;
    }

    public void setDescSubject(String descSubject) {
        this.descSubject = descSubject;
    }

    public void setIdSubject(int idSubject) {
        this.idSubject = idSubject;
    }

    public void setNameSubject(String nameSubject) {
        this.nameSubject = nameSubject;
    }
}
