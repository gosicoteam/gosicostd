package core.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by homax on 09.02.17.
 */
@Entity
@Table(name = "UserGroup")
public class UserGroupModel implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "idUserGroup")
    int idUserGroup;

    @Column(name="nameUserGroup")
    String nameUserGroup;

    @Column(name="descUserGroup")
    String descUserGroup;

    @ManyToMany(
            cascade = {CascadeType.PERSIST, CascadeType.MERGE},
            mappedBy = "groups",
            targetEntity = UsersModel.class,
            fetch = FetchType.EAGER
    )
    Set<UsersModel> users = new HashSet<UsersModel>(0);

    public int getIdUserGroup() {
        return idUserGroup;
    }

    public String getDescUserGroup() {
        return descUserGroup;
    }

    public String getNameUserGroup() {
        return nameUserGroup;
    }

    public Set<UsersModel> getUsers() {
        return users;
    }

    public void setUsers(Set<UsersModel> users) {
        this.users = users;
    }

    public void setDescUserGroup(String descUserGroup) {
        this.descUserGroup = descUserGroup;
    }

    public void setIdUserGroup(int idUserGroup) {
        this.idUserGroup = idUserGroup;
    }

    public void setNameUserGroup(String nameUserGroup) {
        this.nameUserGroup = nameUserGroup;
    }
}
