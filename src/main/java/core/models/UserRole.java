package core.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by homax on 19.03.17.
 */
@Entity
@Table(name = "UserRole")
public class UserRole {

    @Transient
    private int idChair;

    public int getIdChair() {
        return idChair;
    }

    public void setIdChair(int idChair) {
        this.idChair = idChair;
    }

    @Transient
    private String nameGroup;

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    @Transient
    private int group;

    public void setGroup(int group) {
        this.group = group;
    }

    public int getGroup() {
        return group;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "idUserRole")
    private int idUserRole;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "patronicName")
    private String patronicName;

    @ManyToOne
    @JoinColumn(name = "idUser")
    private UsersModel user;

    @ManyToMany(
            targetEntity=AcademicGroup.class,
            cascade={CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER
    )
    @JoinTable(
            name = "UserSpecification",
            joinColumns = @JoinColumn(name = "idUserRole"),
            inverseJoinColumns = @JoinColumn(name = "idAcademicGroup"))
    private Set<AcademicGroup> groups = new HashSet<AcademicGroup>(0);

    public void setUser(UsersModel user) {
        this.user = user;
    }

    public UsersModel getUser() {
        return user;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGroups(Set<AcademicGroup> groups) {
        this.groups = groups;
    }

    public Set<AcademicGroup> getGroups() {
        return groups;
    }

    public void setIdUserRole(int idUserRole) {
        this.idUserRole = idUserRole;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPatronicName(String patronicName) {
        this.patronicName = patronicName;
    }

    public int getIdUserRole() {
        return idUserRole;
    }

    public String getName() {
        return name;
    }

    public String getPatronicName() {
        return patronicName;
    }

    public String getSurname() {
        return surname;
    }
}
