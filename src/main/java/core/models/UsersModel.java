package core.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by homax on 30.10.16.
 */
// Класс, через которого ведётся подключение к базе.
@Entity
@Table(name = "\"User\"")
public class UsersModel implements Serializable {

    @Transient
    int group;

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "idUser")
    private int idUser;

    @Column(name = "password")
    private String password;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "login", unique = true)
    private String login;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "patronicName")
    private String patronicName;

    @Column(name = "dateOfBirth")
    private Date dateOfBirth;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "sex")
    private Boolean sex;

    @Column(name = "activity", columnDefinition="BIT")
    private Boolean activity;

    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true)
    private Set<UserRole> roles = new HashSet<UserRole>();

    @ManyToMany(
            targetEntity=UserGroupModel.class,
            cascade={CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER
    )
    @JoinTable(
            name = "UserGroupSpecification",
            joinColumns = @JoinColumn(name = "idUser"),
            inverseJoinColumns = @JoinColumn(name = "idUserGroup"))
    private Set<UserGroupModel> groups = new HashSet<UserGroupModel>(0);

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public void setPatronicName(String patronicName) {
        this.patronicName = patronicName;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setActivity(Boolean activity) {
        this.activity = activity;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGroups(Set<UserGroupModel> groups) {
        this.groups = groups;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public int getIdUser() {
        return idUser;
    }

    public Set<UserGroupModel> getGroups() {
        return groups;
    }

    public Boolean getSex() {
        return sex;
    }

    public String getEmail() {
        return email;
    }

    public Boolean getActivity() {
        return activity;
    }

    public String getPatronicName() {
        return patronicName;
    }

    public String getPassword() {
        return password;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

}
