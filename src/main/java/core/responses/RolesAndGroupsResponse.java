package core.responses;

import com.fasterxml.jackson.annotation.JsonView;
import core.Helpers.Views;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by homax on 19.03.17.
 */
@ResponseBody
public class RolesAndGroupsResponse {

    @JsonView(Views.Public.class)
    private String idUserRole;

    @JsonView(Views.Public.class)
    private String group;

    @JsonView(Views.Public.class)
    private String student;

    public void setIdUserRole(String idUserRole) {
        this.idUserRole = idUserRole;
    }

    public String getIdUserRole() {
        return idUserRole;
    }

    public String getGroup() {
        return group;
    }

    public String getStudent() {
        return student;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setStudent(String student) {
        this.student = student;
    }
}
