package core.responses;

import com.fasterxml.jackson.annotation.JsonView;
import core.Helpers.Views;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Asus-PC on 06.05.2017.
 */
@ResponseBody
public class TeacherResponse {

    @JsonView(Views.Public.class)
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
