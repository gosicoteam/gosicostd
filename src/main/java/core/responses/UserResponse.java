package core.responses;

import com.fasterxml.jackson.annotation.JsonView;
import core.Helpers.Views;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by homax on 06.02.17.
 */

@ResponseBody
public class UserResponse {

    @JsonView(Views.Public.class)
    String msg;

    @JsonView(Views.Public.class)
    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
