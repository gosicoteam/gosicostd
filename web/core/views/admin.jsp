<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 10.02.17
  Time: 22:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Панель управления</title>

    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/chosen.css" />
    <link href="/resources/CSS/jquery.magicsearch.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/resources/CSS/easydropdown.css" />

    <spring:url value="/resources/js/jquery-3.1.1.min.js" var="jqueryJs" />
    <script src="${jqueryJs}"></script>
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-1.12.4.js">
    </script>
    <script type="text/javascript" language="javascript" src="/resources/js/jquery.dataTables.min.js">
    </script>
    <script src='/resources/js/chosen.jquery.js'></script>
    <script src="/resources/js/jquery.magicsearch.min.js"></script>
    <script src="/resources/js/jquery.easydropdown.min.js"></script>

</head>
<body>
<div id="header">
    <div id="hat-menu">
        <form action="/mainPanel" method="get">
            <button type="submit" class="logoBut"></button>
        </form>
        <div id="adminButPanel" align="center">
            <ul class="tabs_menu">
                <li><a href="#contentAdmin" class="iBut active" id="activeMenuBut"><i class="icon-ok"></i>Активация</a></li>
                <li><a href="#contentAdminTeachers" class="iBut" id="teacherMenuBut"><i class="icon-briefcase"></i>Преподаватели</a></li>
                <li><a href="#contentAdminStudents" class="iBut" id="studentMenuBut"><i class="icon-user"></i>Студенты</a></li>
                <li><a href="#contentAdminDisciplines" class="iBut" id="disciplineMenuBut"><i class="icon-book"></i>Дисциплины</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="tab" id="contentAdmin" align="center">  <%--Активация пользователей--%>
    <div class="search" align="center">
        <form  action="/admin" method="get">
            <input type="text" size="25" name="searchParam">
            <button type="submit" class="iBut"><i class="icon-search"></i>Поиск</button>
            <%--<a href="#" class="updateBut"><i class="icon-search"></i>Поиск</a>--%>
        </form>
    </div>
    <div class="contentTable">
        <form id="usersTable" class="adminTables">
            <table align="center">
                <tr>
                    <td class="TableLines"><h3>Логин</h3></td>
                    <td class="TableLines"><h3>Имя, Фамилия</h3></td>
                    <td class="TableLines"><h3>Активность</h3></td>
                    <td class="TableLines"><h3>Роли</h3></td>
                    <td class="TableLines"><h3>Добавить роль</h3></td>
                </tr>
                <c:forEach items="${users}" var = "user" >
                    <tr id="userRow${user.idUser}">
                        <td class="TableLines"><c:out value="${user.login}"/></td>
                        <td class="TableLines"><c:out value="${user.name} ${user.surname}"/></td>
                        <td>
                            <c:if test="${user.activity == true}">
                                <input  type="checkbox" class="checkbox" name="activity" id="activity${user.idUser}" value="true" checked onchange="$(this).attr('value', this.checked ? true : false); sendUpdatingUserAjax(${user.idUser})"/>
                                <label for="activity${user.idUser}"></label>
                            </c:if>
                            <c:if test="${user.activity != true}">
                                <input type="checkbox" class="checkbox" name="activity" id="activity${user.idUser}" value="false" onchange="$(this).attr('value', this.checked ? true : false); sendUpdatingUserAjax(${user.idUser})"/>
                                <label for="activity${user.idUser}"></label>
                            </c:if>
                        </td>
                        <%--<td>--%>
                            <%--<button type="submit" class="iBut" id="updateUser" onclick="sendUpdatingUserAjax(${user[0]})"><i class="icon-refresh"></i>Обновить</button>--%>
                        <%--</td>--%>

                        <td class="TableLines" id="roles${user.idUser}" style="width: 140px;">
                            <%--<c:forEach items="${user.roles}" var="role">--%>
                                <%--${role.name} ${role.surname};--%>
                            <%--</c:forEach>--%>


                            <select class="userRolesSelect dropdown" id="userRolesSelect${user.idUser}">
                                <option selected value="0">Убрать роль</option>
                                <c:forEach items="${user.roles}" var="role">
                                    <option value="${role.idUserRole}">${role.name} ${role.surname};
                                </c:forEach>
                            </select>

                        </td>
                        <td>
                            <input class="userRoles" id="userRole${user.idUser}" placeholder="Выберите роль">
                        </td>

                        <td>
                            <button type="submit" class="iBut" id="deleteUser" onclick="sendDeletingUserAjax(${user.idUser})"><i class="icon-remove"></i>Удалить</button>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </form>
    </div>
</div>

<div class="tab" id="contentAdminTeachers" align="center">  <%--Страница преподавателей--%>
    <div class="contentTable">
        <form id="teachersTable" class="adminTables teachersTable tableAdm">
            <%--<table align="center">--%>
                <%--<tr>--%>
                    <%--<td class="TableLines"><h3>ФИО</h3></td>--%>
                    <%--<td class="TableLines"><h3>Преподаваемые дисциплины</h3></td>--%>
                    <%--<td class="TableLines"><h3>Обучаемые группы</h3></td>--%>
                    <%--<td class="TableLines"><h3></h3></td>--%>

                <%--</tr>--%>
            <%--</table>--%>
                <table align="center" id="tableT" class="tableAdm">
                    <thead>
                        <tr>
                            <th class="TableLines">ФИО</th>
                            <%--<th class="TableLines">Преподаваемые дисциплины</th>--%>
                            <%--<th class="TableLines">Обучаемые группы</th>--%>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
        </form>
    </div>
    <div class="adminLeftMenu">

        <h4>Кафедра</h4>
        <select class="selectChairs" id="selectChairsTeachers" size="1" style="width:150px">
            <option selected value="0">Выберите кафедру
        </select>
    </div>
    <div class="adminRightMenu">
        <ul class="tabs_menu">
            <li><a href="#addTeacher" class="iBut" id="addTeacherBut"><i class="icon-plus"></i>Добавить преподавателя</a></li>
        </ul>
    </div>
</div>

<div class="tab" id="contentAdminStudents" align="center">  <%--Страница студентов--%>
    <div class="contentTable">
        <form id="studentsTable" class="adminTables studentsTable tableAdm">
            <%--<table align="center">--%>
                <%--<tr>--%>
                    <%--<td class="TableLines"><h3>Учебная группа</h3></td>--%>
                    <%--<td class="TableLines"><h3>Студенты</h3></td>--%>
                <%--</tr>--%>
            <%--</table>--%>

            <table align="center" id="tableS" class="tableAdm">
                <thead>
                    <tr>
                        <th class="TableLines">Учебная группа</th>
                        <th class="TableLines">Студенты</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

        </form>
    </div>
    <div class="adminLeftMenu">
        <h4>Кафедра</h4>
        <select class="selectChairs" id="selectChairsStudent" size="1" style="width:150px">
            <option selected value="0">Выберите кафедру
        </select>
    </div>
    <div class="adminRightMenu">
        <ul class="tabs_menu">
            <li><a href="#addStudent" class="iBut" id="addStudentBut"><i class="icon-plus"></i>Добавить студента</a></li>
        </ul>
    </div>
</div>

<div class="tab" id="contentAdminDisciplines" align="center"> <%--Страница дисциплин--%>
    <div class="contentTable">
        <form id="disciplineTable" class="adminTables teachersTable tableAdm">
            <%--<table align="center">--%>
                <%--<tr>--%>
                    <%--<td class="TableLines"><h3>Название учебной дисциплины</h3></td>--%>
                <%--</tr>--%>
            <%--</table>--%>
                <table align="center" id="tableD" class="tableAdm">
                    <thead>
                        <tr>
                            <th class="TableLines">Название учебной дисциплины</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

        </form>
    </div>
    <div class="adminLeftMenu">

        <h4>Кафедра</h4>
        <select class="selectChairs" id="selectChairsDisciplines" size="1" style="width:150px">
            <option selected value="0">Выберите кафедру
        </select>
    </div>
    <div class="adminRightMenu">
        <ul class="tabs_menu">
            <li><a href="#addDiscipline" class="iBut" id="addDisciplineBut"><i class="icon-plus"></i>Добавить дисциплину</a></li>
        </ul>
    </div>
</div>

<div class="tab" id="addTeacher" align="center">  <%--Страница добавления преподавателя--%>
    <form class="tables">

        <table align="center" border="0">

            <h2 class="header1"><font face="Century Gothic" size=5 color="white">Введите данные о преподавателе</font></h2>

            <tr>
                <td class="TableLines" align="right" valign="top">Кафедра: </td>
                <td class="TableLines">
                    <select id="selectChairsTeacher" class="selectChairs clearValSelect" size="1" style="width:190px">
                        <option value="0">Выберите кафедру
                    </select></td>
                <td class="TableLines"><span class="ErrorMessage"  id='depTeachF'></span></td>
            </tr>
            <tr>
                <td class="TableLines"  align="right" valign="top">Фамилия: </td>
                <td class="TableLines"><input class="clearVal" type="text" id="surnameTeacher" size="25" required></td>
                <td class="TableLines"><span class="ErrorMessage" id='surnameTeacherF'></span></td>
            </tr>
            <tr>
                <td class="TableLines"  align="right" valign="top">Имя: </td>
                <td class="TableLines"><input class="clearVal" type="text" id="nameTeacher" size="25" required></td>
                <td class="TableLines"><span class="ErrorMessage" id='nameTeacherF'></span></td>
            </tr>
            <tr>
                <td class="TableLines"  align="right" valign="top">Отчество: </td>
                <td class="TableLines"><input class="clearVal" type="text" id="patronicnameTeacher" size="25" required></td>
                <td class="TableLines"><span class="ErrorMessage" id='patronicnameTeacherF'></span></td>
            </tr>
            <tr>
                <td colspan="2">
                    <a href="#" id = "addTeacherForm" class="iBut addButtons"><i class="icon-plus"></i>Добавить преподавателя</a>
                </td>
            </tr>
        </table>
    </form>
</div>

<div class="tab" id="addStudent" align="center">  <%--Страница добавления студента--%>
    <form class="tables">

        <table align="center" border="0">

            <h2 class="header1"><font face="Century Gothic" size=5 color="white">Введите данные о студенте</font></h2>

            <tr>
                <td class="TableLines" align="right" valign="top">Кафедра: </td>
                <td class="TableLines">
                    <select id="selectChairStudent" class="selectChairs clearValSelect" size="0" style="width:190px">
                        <option selected value="0">Выберите кафедру
                    </select></td>
                <td class="TableLines"><span class="ErrorMessage" id='depStudF'></span></td>
            </tr>
            <tr>
                <td class="TableLines" align="right" valign="top">Фамилия: </td>
                <td class="TableLines"><input class="clearVal" id="SecondNameInput" type="text" size="25" required></td>
                <td class="TableLines"><span class="ErrorMessage" id='SecondNameInputF'></span></td>
            </tr>
            <tr>
                <td class="TableLines" align="right" valign="top">Имя: </td>
                <td class="TableLines"><input class="clearVal" id="NameInput" type="text" size="25" required></td>
                <td class="TableLines"><span class="ErrorMessage" id='NameInputF'></span></td>
            </tr>
            <tr>
                <td class="TableLines" align="right" valign="top">Отчество: </td>
                <td class="TableLines"><input class="clearVal" id="MiddleNameInput" type="text" size="25" required></td>
                <td class="TableLines"><span class="ErrorMessage" id='MiddleNameInputF'></span></td>
            </tr>
            <td class="TableLines" align="right" valign="top">Выберите группу: </td>
            <td class="TableLines">
                <select id="chooseGroup" size="1" class="otherFieldOption clearValSelect">
                    <option value="0">Выберите группу
                </select>
            </td>
            </tr>
            <tr class="TableLines" id="otherField" name="otherField">
                <td class="TableLines" align="right" valign="top">Создать новую группу: </td>
                <td class="TableLines"><input class="clearVal" type="text" id="newGroup" placeholder="  1ААА-111" size="25"></td>
                <td class="TableLines"><span class="ErrorMessage" id='newGrF'></span></td>
            </tr>

            <tr>
                <td colspan="2">
                    <a href="#" class="iBut addButtons" id="addStudentForm"><i class="icon-plus"></i>Добавить студента</a>
                </td>
            </tr>
        </table>
    </form>
</div>

<div class="tab" id="addDiscipline" align="center">  <%--Страница добавления дисциплины--%>
    <form class="tables">

        <table align="center" border="0">

            <h2 class="header1"><font face="Century Gothic" size=5 color="white">Введите данные об учебной дисциплине</font></h2>

            <tr>
                <td class="TableLines" align="right" valign="top">Кафедра: </td>
                <td class="TableLines">
                    <select class="selectChairs clearValSelect" id="selectChairsInTeacherTab" size="1" style="width:190px">
                        <option value="0">Выберите кафедру
                    </select></td>
                <td class="TableLines"><span class="ErrorMessage" id='depDisF'></span></td>
            </tr>
            <tr>
                <td class="TableLines" align="right" valign="top">Наименование: </td>
                <td class="TableLines"><input class="clearVal" type="text" id="nameOfDiscipline" size="30" required style="width:190px"></td>
                <td class="TableLines"><span class="ErrorMessage" id='nameOfDisciplineF'></span></td>
            </tr>
            <tr>
                <td class="TableLines" align="right" valign="top">Прикрепить преподавателя: </td>
                <td class="TableLines">
                    <select class="clearValSelect" id="teachersSelect" size="1" style="width:190px"  multiple="true">
                    </select>
                </td>
            </tr>

            <tr>
                <td class="TableLines" align="right" valign="top">Прикрепить группу: </td>
                <td class="TableLines">
                    <select class="clearValSelect" id="groopsSelect" size="1" style="width:190px"  multiple="true">
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a href="#" id="addDisciplineForm" class="iBut addButtons"><i class="icon-plus"></i>Добавить дисциплину</a>
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="footer"></div>




<script>

//    $(document).ready(function(){
//        //Chosen
//        $("#teachersSelect").chosen({
//            placeholder: "Выберите преподавателя"
//        })
//    })

//    $("#teachersSelect").on('change', function(){
//        $("#listOfTeachers").add($(this).val())
//    })

    $(document).ready(function($){
        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "/get/chairs",
            dataType : 'json',
            timeout : 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);
                var selectControls = $('.selectChairs');
                for(var i = 0; i < selectControls.length; i++)
                {
                    for(var key in data)
                    {
                        selectControls[i].innerHTML += "<option value=" + "\"" + key + "\">" + data[key];
                    }
                }

            },
            error : function(e) {
                console.log("ERROR: ", e);

            },
            done : function(e) {
                console.log("DONE");
            }
        });

        // Обработчики
        $("#deleteUser").submit(function(event) {
            event.preventDefault();
        });

        $("#updateUser").submit(function(event) {
            event.preventDefault();
        });

        $("#selectChairsInTeacherTab").change(function(){
           var idChair = $('#selectChairsInTeacherTab').val();

           requestTeachersOfChair(idChair);
        });

//        $('#teachersSelect').change(function(){
//            var idTeacher = $('#teachersSelect').val();
//            var list = document.getElementById('listOfTeachers');
//            var listIds = document.getElementById('listOfIdTeachers');
//
//            if(idTeacher != 0) {
//                 if(list.value == null || list.value == '')
//                 {
//                     var sel = document.getElementById("teachersSelect");
//                     list.value += sel.options[sel.selectedIndex].text;
//                     listIds.value += sel.value;
//                 }
//                 else
//                 {
//                     var sel = document.getElementById("teachersSelect");
//                     list.value += '; ' + sel.options[sel.selectedIndex].text;
//                     listIds.value += '; ' + sel.value;
//                 }
//            }
//        });

        $("#selectChairStudent").change(function(){
            var idChair = $('#selectChairStudent').val();

            requestGroupsOfChair(idChair);
        });

        $("#selectChairsStudent").change(function () {
            createStudentTable(1);
        });

        $("#selectChairsTeachers").change(function () {
            createTeachersTable(1);
        });

        $("#selectChairsDisciplines").change(function () {
            createDisciplinesTable(1);
        });

        $("#addDisciplineForm").click(function(event)
        {
            event.preventDefault();

            var name = $('#nameOfDiscipline').val();
            var ids = $("#teachersSelect").chosen().val();
            var idChair = $('#selectChairsInTeacherTab').val();
            var teachers = "" ;
            ids.forEach(function(item)
                {
                    if(ids[ids.length - 1] == item) {
                        teachers += item
                    }
                    else {
                        teachers += item + ";"
                    }

                });
            console.log(teachers);
            var IsValid = true;  //Задаем переменную валидации формы

            //Очистка полей сообщений об ошибочном заполнении формы

            document.getElementById('nameOfDisciplineF').innerHTML = '';
            document.getElementById('depDisF').innerHTML = '';
            //Условия и вывод сообщений ошибки, при неккоректном заполнении формы

            if($('#selectChairsInTeacherTab').val() == 0)
            {
                document.getElementById('depDisF').innerHTML = '*Выберите кафедру';
                IsValid = false;
//                alert('Укажите кафедру');
//                return;
            }

            if ($('#nameOfDiscipline').val() == "") {
                document.getElementById('nameOfDisciplineF').innerHTML = '*Введите наименование';
                IsValid = false;
            }

            if(!IsValid){
                return;
            }


            $.ajax({
                type: "POST",
                url: "/add/dicsipline",
                data: { name: name, teachers: teachers, idChair: idChair },
                timeout: 100000,
                success : function(data) {
                    alert('Дисциплина успешно довалена');
                    $('.clearVal').val('');
                    $('.clearValSelect').val(0);
                    console.log("SUCCESS: ", data);

                    $("#teachersSelect").chosen({
                        placeholder_text_multiple: "Выберите преподавателя"
                    })

                },
                error: function(data) {
                    console.log("ERROR: ", data);
                },
                done: function(data) {
                    console.log("DONE: ", data);
                }

            });

        });

        $("#addTeacherForm").click(function(event) {
            event.preventDefault();



            var name = $('#nameTeacher').val();
            var surname = $('#surnameTeacher').val();
            var patronicName = $('#patronicnameTeacher').val();
            var groupName = $('#newGroup').val();
            var idChair = $('#selectChairsTeacher').val();
            var IsValid = true;  //Задаем переменную валидации формы

            //Очистка полей сообщений об ошибочном заполнении формы

            document.getElementById('surnameTeacherF').innerHTML = '';
            document.getElementById('nameTeacherF').innerHTML = '';
            document.getElementById('patronicnameTeacherF').innerHTML = '';
            document.getElementById('depTeachF').innerHTML = '';
            //Условия и вывод сообщений ошибки, при неккоректном заполнении формы

            if($('#selectChairsTeacher').val() == 0)
            {
                document.getElementById('depTeachF').innerHTML = '*Выберите кафедру';
                IsValid = false;
//                alert('Укажите кафедру');
//                return;
            }

            if (surname == "") {
                document.getElementById('surnameTeacherF').innerHTML = '*Введите фамилию';
                IsValid = false;
            }

            if (name == "") {
                document.getElementById('nameTeacherF').innerHTML = '*Введите имя';
                IsValid = false;
            }

            if (patronicName == "") {
                document.getElementById('patronicnameTeacherF').innerHTML = '*Введите отчество';
                IsValid = false;
            }

            if(!IsValid){
                return;
            }

            $.ajax({
                type : "POST",
                url : "/users/addTeacher",
                data : {
                    name: name,
                    surname: surname,
                    patronicName: patronicName,
                    idChair: idChair
                },
                timeout : 100000,
                success : function(data) {
                    alert('Преподаватель успешно добавлен');
                    $('.clearVal').val('');
                    $('.clearValSelect').val(0);
                    console.log("SUCCESS: ", data);
                },
                error : function(e) {
                    console.log("ERROR: ", e);
                },
                done : function(e) {
                    console.log("DONE");
                }
            });

        });

        $("#addStudentForm").click(function(event) {
            event.preventDefault();

            var data = {};
            data["name"] = $('#NameInput').val();
            data["surname"] = $('#SecondNameInput').val();
            data["patronicName"] = $('#MiddleNameInput').val();
            var group = $('#chooseGroup').val();

            var IsValid = true;  //Задаем переменную валидации формы

            //Очистка полей сообщений об ошибочном заполнении формы

            document.getElementById('SecondNameInputF').innerHTML = '';
            document.getElementById('NameInputF').innerHTML = '';
            document.getElementById('MiddleNameInputF').innerHTML = '';
            document.getElementById('depStudF').innerHTML = '';
            document.getElementById('newGrF').innerHTML = '';
            //Условия и вывод сообщений ошибки, при неккоректном заполнении формы

            if($('#selectChairStudent').val() == 0)
            {
                document.getElementById('depStudF').innerHTML = '*Выберите кафедру';
                IsValid = false;
//                alert('Укажите кафедру');
//                return;
            }

            if ($('#SecondNameInput').val() == "") {
                document.getElementById('SecondNameInputF').innerHTML = '*Введите фамилию';
                IsValid = false;
            }

            if ($('#NameInput').val() == "") {
                document.getElementById('NameInputF').innerHTML = '*Введите имя';
                IsValid = false;
            }

            if ($('#MiddleNameInput').val() == "") {
                document.getElementById('MiddleNameInputF').innerHTML = '*Введите отчество';
                IsValid = false;
            }
//            alert($('#newGroup').val());
//            alert ($('#chooseGroup').val());

            if ($('#newGroup').val() == "" && $('#chooseGroup').val() == 0) {
                document.getElementById('newGrF').innerHTML = '*Создайте группу или выберите из списка';
                IsValid = false;
            }


            if(!IsValid){
                return;
            }

            if(group == null || group == 0)
            {
                data["nameGroup"] = $('#newGroup').val();
                data["idChair"] = $('#selectChairStudent').val();
            }
            else
            {
                data["group"] = group;
            }



            $.ajax({
                type : "POST",
                contentType : "application/json",
                url : "/users/add",
                data : JSON.stringify(data),
                timeout : 100000,
                success : function(data) {
                    console.log("SUCCESS: ", data);
                    alert("Студент успешно добавлен");
                    $('.clearVal').val('');
                    $('.clearValSelect').val(0);
                    document.getElementById('NameInput').innerHTML = '';
                    document.getElementById('SecondNameInput').innerHTML = '';
                    document.getElementById('MiddleNameInput').innerHTML = '';
                },
                error : function(e) {
                    console.log("ERROR: ", e);
                },
                done : function(e) {
                    console.log("DONE");
                }
            });

        });
    });

    function requestTeachersOfChair(idChair)
    {
        $.ajax({
            type : "POST",
            url : "/get/teachers",
            data: { idChair: idChair },
            timeout : 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);
                var selectControls = document.getElementById("teachersSelect");

                selectControls.innerHTML="";

                for(var key in data)
                {
                    selectControls.innerHTML += "<option value=" + "\"" + key + "\">" + data[key];
                }

                $("#teachersSelect").trigger("chosen:updated");
                $("#teachersSelect").chosen({
                    placeholder_text_multiple: "Выберите преподавателя"
                })
                $("#groopsSelect").chosen({
                    placeholder_text_multiple: "Выберите группу"
                })
            },
            error : function(e) {
                console.log("ERROR: ", e);

            },
            done : function(e) {
                console.log("DONE");
            }
        });
    }

    function requestGroupsOfChair(idChair) {

        $.ajax({
            type : "POST",
            url : "/get/groups",
            data: { idChair: idChair },
            timeout : 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);
                var selectControls = document.getElementById("chooseGroup");
                selectControls.innerHTML = "<option selected value=0>Выберите группу"
                for(var key in data)
                {
                    selectControls.innerHTML += "<option value=" + "\" " + key + "\">" + data[key];
                }

            },
            error : function(e) {
                console.log("ERROR: ", e);

            },
            done : function(e) {
                console.log("DONE");
            }
        });

    }

    function sendUpdatingUserAjax(id) {

        var data = {}
        data["idUser"] = id;
        data["activity"] = document.getElementById("activity" + id).value;

        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "/users/update",
            data : JSON.stringify(data),
            dataType : 'json',
            timeout : 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);

            },
            error : function(e) {
                console.log("ERROR: ", e);

            },
            done : function(e) {
                console.log("DONE");
            }
        });
    }

    function sendDeletingUserAjax(id) {
        var data = {}
        data["idUser"] = id;

        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "/users/delete",
            data : JSON.stringify(data),
            dataType : 'json',
            timeout : 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);
                if (data["code"] == 200) {
                    var parent = deleteElementById("userRow" + id);
                }
                else {

                }

            },
            error : function(e) {
                console.log("ERROR: ", e);

            },
            done : function(e) {
                $("#updateUser").prop("disabled", true);
                console.log("DONE");
            }
        });
    }

    function deleteElementById(id) {
        var contentReg = document.getElementById(id);
        var parent = contentReg.parentNode;
        parent.removeChild(contentReg);
        return parent;
    }

    <%--Функция переключения страниц--%>

    $(document).ready(function(){
        $('.tabs_menu a').click(function(e) {       <%--Отслеживание клика--%>
                e.preventDefault();                     <%--Отмена стандартного поведения кнопки--%>
                $('.tabs_menu .active').removeClass('active');    <%--Выделение активной вкладки. Удаление текущего аргумента active--%>
                $(this).addClass('active');                     <%--Добавление класса active в текущую вкладку--%>
                var tab = $(this).attr('href');       <%--Вывод контента, в зависимости от выбора--%>
                $('.tab').not(tab).css({'display':'none'});     <%--Скрытие контента блоков, кроме выбранного--%>
                $(tab).fadeIn(400);             <%--Вывод контента выбранного блока с задержкой в 400млсек--%>
        });
    });

    <%--Функция показа и скрытия поля формы--%>

    $(document).ready(function() {
        $.viewInput = {'0' : $([]), '0' : $('#otherField'),    //Это имя вокруг невидимого поля
        };

        $('.otherFieldOption').change(function() {
            $.each($.viewInput, function() {
                this.hide(); }); // Прячет это поле, если выбрана другая опция
            if ($(this).val() == 0) {
                //alert("nothing was selected");
                $.each($.viewInput, function() {
                    this.show(); }); // Показывает это поле, если выбрана другая опция
            }
        });
    });

//

$(document).ready(function() {

    $('.userRolesSelect').change(function() {
        console.log(this.value + ' ' + this.id.match(/\d/g).join(""));

        var idUser = this.id.match(/\d/g).join("");
        var idUserRole = this.value;
        alert(idUser + ' ' + idUserRole);

        $.ajax({
            type: "POST",
            url: "/user/disattach",
            data: { idUser: idUser, idUserRole: idUserRole},
            timeout: 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);
            },
            error: function(data) {
                console.log("ERROR: ", data);
            },
            done: function(data) {
                console.log("DONE: ", data);
            }
        });

        location.reload();
    });


    //    var roles = document.getElementsByClassName('userRolesSelect');
//
//    for(var i = 0; i < roles.length; i++)
//    {
//        var idRole = roles[i].id;
//
//        $('#' + idRole).change(function() {
//            alert(idRole);
//        });
//    }
});
    <%--Таблицы--%>



    function createStudentTable(reload) {

        if(reload == 1)
        {
            var table = $('#tableS').dataTable();
            table.api().ajax.reload();
        }
        else {
            $('#tableS').DataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                ajax: {
                    type: "POST",
                    url: '/get/student_and_groups',
                    data: function (d) {
                        d.idChair = $('#selectChairsStudent').val();
                        console.log(d);
                    },
                    dataSrc: ""

                },
                columns: [
                    {"data": "group"},
                    {"data": "student"}
                ],
                language: {
                    "processing": "Подождите...",
                    "search": "Поиск: ",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            });
        }
    }

    function createTeachersTable(reload) {

        if(reload == 1)
        {
            var table = $('#tableT').dataTable();
            table.api().ajax.reload();
        }
        else {
            $('#tableT').DataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                ajax: {
                    type: "POST",
                    url: '/get/teachers_groups',
                    data: function (d) {
                        d.idChair = $('#selectChairsTeachers').val();
                        console.log(d);
                    },

                    dataSrc: ""

                },
                columns: [
                    {"data": "name"}
//                    {"data": "disciplines"},
//                    {"data": "groups"}
                ],
                language: {
                    "processing": "Подождите...",
                    "search": "Поиск: ",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            });
        }
    }

    function createDisciplinesTable(reload) {

        if(reload == 1)
        {
            var table = $('#tableD').dataTable();
            table.api().ajax.reload();
        }
        else {
            $('#tableD').DataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                ajax: {
                    type: "POST",
                    url: '/get/disciplines',
                    data: function (d) {
                        d.idChair = $('#selectChairsDisciplines').val();
                        console.log(d);
                    },
                    dataSrc: ""

                },
                columns: [
                    {"data": "name"}
                ],
                language: {
                    "processing": "Подождите...",
                    "search": "Поиск: ",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            });
        }
    }

    $(document).ready(function(){

        createStudentTable(0);
        createDisciplinesTable(0);
        createTeachersTable(0);

        var dataSource = [
            {id: 1, firstName: 'Tim', lastName: 'Cook'},
            {id: 2, firstName: 'Eric', lastName: 'Baker'},
            {id: 3, firstName: 'Victor', lastName: 'Brown'},
            {id: 4, firstName: 'Lisa', lastName: 'White'},
            {id: 5, firstName: 'Oliver', lastName: 'Bull'},
            {id: 6, firstName: 'Zade', lastName: 'Stock'},
            {id: 7, firstName: 'David', lastName: 'Reed'},
            {id: 8, firstName: 'George', lastName: 'Hand'},
            {id: 9, firstName: 'Tony', lastName: 'Well'},
            {id: 10, firstName: 'Bruce', lastName: 'Wayne'}
        ];

        var roles = document.getElementsByClassName('userRoles');
        for(var i = 0; i < roles.length; i++)
        {
            var idRole = roles[i].id;

            $('#' + idRole).magicsearch({
                type: 'ajax',
                ajaxOptions: {
                    success: function(data){
                        console.log(data);
                    },
                    error: function(data){
                        console.log(data);
                    }

                },
                dataSource: '/get/student_and_groups',
                fields: [ 'idUserRole', 'student', 'group'],
                id: 'idUserRole',
                format: '%student% · %group%',
                dropdownBtn: true,
                dropdownMaxItem: 10,
                multiple: false,
                success: function($input, data) {
                    var idUser = $input[0].id.match(/\d/g).join("");
                    var idUserRole = data['idUserRole'];
//                    alert(idUser + ' ' + idUserRole);

                    $.ajax({
                        type: "POST",
                        url: "/user/attach",
                        data: { idUser: idUser, idUserRole: idUserRole},
                        timeout: 100000,
                        success : function(data) {
                            console.log("SUCCESS: ", data);
                        },
                        error: function(data) {
                            console.log("ERROR: ", data);
                        },
                        done: function(data) {
                            console.log("DONE: ", data);
                        }
                    });

                    location.reload();
                    return true;
                },
                afterDelete: function($input, data) {
                    alert('into afterDelete '  + 'id = ' + data['id']);
                    location.reload();
                    return true;
                }

        });
        }



    });

</script>


</body>
</html>
