<%--
  Created by IntelliJ IDEA.
  User: Asus-PC
  Date: 03.02.2017
  Time: 23:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Восстановление пароля</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
</head>
<body>
<div id="forgotPass">
    <div id="header">
        <div id="hat-menu">
            <form action="/" method="get">
                <button type="submit" class="logoBut"></button>
            </form>
        </div>
    </div>
    <div id="contentForgot" align="center">
        <p class="forgot">Введите адрес электронной почты</p>
        <form>
            <input class="forgotEmail" type="text" name="e-mail" size="25">
            <button  type="submit" value="Login" class="forgotBut"></button>
        </form>
    </div>
    <div id="footer"></div>
</div>

</body>
</html>