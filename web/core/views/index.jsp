<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
  <head>
    <title>Добро пожаловать в Gosico</title>
      <link rel="stylesheet" type="text/css" href="/resources/CSS/style.css" />
      <link rel="stylesheet" type="text/css" href="/resources/CSS/animate.css" />
  </head>
    <body>
        <div id="autorization">
            <div id="header-welcome" class="animated fadeIn"></div>
            <div id="line" class="bottomElm animated slideInUp"></div>
                <div id="content" align="center">
                    <security:authorize access="hasAnyRole('Admin', 'Student')" var="isUSer"/>
                    <c:if test="${not isUSer}">
                    <form method="POST" action="<spring:url value="/j_spring_security_check" />" class="form" >

                        <p class="field">
                          <input type="text" name="j_username" placeholder="  Логин" class="login animated fadeInUp">
                        </p>

                        <p class="field">
                              <input type="password" name="j_password" placeholder="  Пароль" class="pass animated fadeInUp">
                        </p>
                       <button  type="submit" value="Login" class="buttLog bottomElm animated slideInUp"></button><br><br>

                    </form>
                        <div id="links" align="center" class="bottomElm animated slideInUp">
                            <a href="/users/create">Регистрация в Gosico</a>
                            <a href="/forgotpass">Забыли пароль?</a>
                        </div>
                    </c:if>
                    <c:if test="${isUSer}">
                        <form action="/mainPanel"><button type="submit" value="Login"  class="button1" ></button></form>
                        <form action="/j_spring_security_logout"><button type="submit" class="button2"></button></form>
                    </c:if>

                    <% if(request.getParameter("error") != null) { %>
                        <p id="fail-message" align="center"><font face="Century Gothic" size=4 color="red">Неверный логин или пароль.</font><br/></p>
                    <% } %>

                </div>
            <div id="right-menu"></div>
        <div id="left-menu"></div>
    <div id="footer"></div>
        </div>
    </body>
</html>
