<%@ page import="core.models.UsersModel" %><%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 30.10.16
  Time: 22:49
  To change this template use File | Settings | File Templates.
  основная панель пока с ней много вопросов
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Главная</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/animate.css" />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/easydropdown.css" />
    <script src="/resources/js/jquery-3.1.1.min.js"></script>
    <script src="/resources/js/jquery.easydropdown.min.js"></script>

</head>
<body>

<%
    UsersModel user = (UsersModel)session.getAttribute("currentUser");
    int currentUserId = user.getIdUser();
%>

<div id="mainPanel">
    <div id="header">
        <div id="hat-menu">

            <form action="/" method="get">
                <button type="submit" class="logoBut"></button> <br>
            </form>
                <div id="user"   class="animated fadeInRight">

                    <% if(currentUserId == 1) { %>
                        <div class="userInf">
                            <p class="rightBar" align="center" >Здравствуйте,<br> <a href="/admin"><c:out value="${userName}"/></a></p>
                        </div>
                        <div class="divIconUs">
                            <form action="/admin" method="get" class="userIcon"><button type="submit"></button></form>
                        </div>
                    <% } else { %>
                        <div class="userInf">
                            <p class="rightBar" align="center" >Здравствуйте,<br> <a href="/users/form/"><c:out value="${userName}"/></a></p>
                        </div>
                        <div class="divIconUs">
                            <form action="/users/form/" method="get" class="userIcon"><button type="submit"></button></form>
                        </div>
                    <% } %>
                </div>
        </div>
    </div>
    <div id="roleSelect" style="width: 200px; float: right; margin: 0.5% 0.5%" class="animated fadeIn">
        <c:if test="${currentRole != null && currentRole != 0}">
            <select id="selectRole" size="1" class="dropdown">
                <option value="0">Выберите роль
                    <c:forEach items="${currentUser.roles}" var="role">
                    <c:if test="${role.idUserRole == currentRole}">
                <option selected value="${role.idUserRole}">${role.surname} ${role.name}
                    </c:if>
                    <c:if test="${role.idUserRole != currentRole}">
                <option value="${role.idUserRole}">${role.surname} ${role.name}
                    </c:if>
                    </c:forEach>
            </select>
        </c:if>
        <c:if test="${currentRole == null || currentRole == 0}">
            <select id="selectRole" size="1" class="dropdown">
                <option selected value="0">Выберите роль
                    <c:forEach items="${currentUser.roles}" var="role">
                <option value="${role.idUserRole}">${role.surname} ${role.name}
                    </c:forEach>
            </select>
        </c:if>
    </div>
        <div id="contentMainPanel">


            <form id="menu" class="animated fadeIn">

                    <table align="center" border="0">

        <tr>
            <td><button class="menuAnimate" data-effect="pulse" type="submit" id="butcalendar" formaction="/apps/Calendar" formmethod="get"></button></td>
            <style>#butcalendar{
                background: url("/resources/modules/Calendar/icon.png") center / 100% 100%;
            }</style>
            <td><button class="menuAnimate" data-effect="pulse" type="submit" id="butschedule" formaction="/apps/Schedule" formmethod="get"></button></td>
            <style>#butschedule{
                background: url("/resources/modules/Schedule/icon.png") center / 100% 100%;
            }</style>
            <td><button class="menuAnimate" data-effect="pulse" type="submit" id="butnotes" formaction="/apps/Notes" formmethod="get"></button></td>
            <style>#butnotes{
                background: url("/resources/modules/Notes/icon.png") center / 100% 100%;
            }</style>
            <td><button class="menuAnimate" data-effect="pulse" type="submit" id="butreferenceMat" formaction="/apps/ReferenceMaterials" formmethod="get"></button></td>
            <style>#butreferenceMat{
                background: url("/resources/modules/ReferenceMaterials/icon.png") center / 100% 100%;
            }</style>
        </tr>
        </table>

        <table align="center" border="0" cellpadding="30">
            <tr>
                <td><button class="menuAnimate" data-effect="pulse" type="submit" id="butEJ" formaction="/apps/EJ" formmethod="get"></button></td>
                <style>#butEJ{
                    background: url("/resources/modules/EJ/icon.png") center / 100% 100%;
                }</style>
                <td><button <%--class="menuAnimate" data-effect="pulse"--%> type="submit" id="butmessages" <%--formaction="/apps/Messages"--%> formmethod="get"></button></td>
                <style>#butmessages{
                    background: url("/resources/modules/Messages/noActiveMas.png") center / 100% 100%;
                    pointer-events: none; /* делаем ссылку некликабельной */
                    cursor: default;  /* устанавливаем курсор в виде стрелки */
                }</style>
                <td><button class="menuAnimate" data-effect="pulse" type="submit" id="butsearch" formaction="/apps/Search" formmethod="get"></button></td>
                <style>#butsearch{
                    background: url("/resources/modules/Search/icon.png") center / 100% 100%;
                }</style>
                <td><button class="menuAnimate" data-effect="pulse" type="submit" id="butphones" formaction="/apps/Phones" formmethod="get"></button></td>
                <style>#butphones{
                    background: url("/resources/modules/Phones/icon.png") center / 100% 100%;
                }</style>
            </tr>
        </table>

                </form>
            </div>
        <div id="footer"></div>
    </div>

<%--Анимация наведения курсора мыши--%>

<script>

function animate(elem){
    var effect = elem.data("effect");
    elem.addClass("animated " + effect).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        elem.removeClass("animated " + effect);
    });
}

$(document).ready(function(){
    $(".menuAnimate").mouseenter(function(){
        animate($(this));
    });

    $("#selectRole").change(function () {

        var idUserRole = $("#selectRole").val();

        $.ajax({
            type: "POST",
            url: "/mainPanel/setRole",
            data: { idUserRole: idUserRole},
            timeout: 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);
            },
            error: function(data) {
                console.log("ERROR: ", data);
            },
            done: function(data) {
                console.log("DONE: ", data);
            }

        });

    });

});

</script>



</body>
</html>