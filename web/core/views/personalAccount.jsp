<%@ page import="core.models.UsersModel" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="core.models.UserGroupModel" %>
<%@ page import="java.util.Set" %><%--
  Created by IntelliJ IDEA.
  User: Asus-PC
  Date: 11.03.2017
  Time: 11:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Личный кабинет</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <spring:url value="/resources/js/jquery-3.1.1.min.js" var="jqueryJs" />
    <script src="${jqueryJs}"></script>
</head>
<body>
<c:url var="home" value="/" scope="request" />

<%
    UsersModel user = (UsersModel)session.getAttribute("currentUser");
    DateFormat format = new SimpleDateFormat("dd-MM-YYYY");

    Set<UserGroupModel> groups = user.getGroups();
    String groupsString = "";

    for(UserGroupModel group : groups){
        groupsString += group.getNameUserGroup() + ";";
    }
%>

<div id="personalAccount">

    <div id="header">
        <div id="hat-menu">
            <form action="/mainPanel" method="get">
                <button type="submit" class="logoBut"></button>
            </form>
        </div>
    </div>
    <div id="left-menu">
        <div class="leftBar" align="center">
            <form action="" method="get" class="userIconPA"><button type="submit"></button></form>
            <p>Личный кабинет<br>пользователь<br><a id="UsLinkPA" href=""><%= user.getName() %><br><%= user.getSurname() %></a></p>
            <br><a id="exitSysPA" href="/j_spring_security_logout">Выйти из системы</a>
        </div>
    </div>

    <div id="personalAccountContent" align="center" class="tab tablePA">

            <form id="PA" class="tables">

                <table align="center" border="0">

                    <h2 class="header1"><font face="Century Gothic" size=5 color="white">Личный кабинет</font></h2>

                    <tr>
                        <td class="TableLines" align="right" valign="top">Фамилия: </td>
                        <td class="TableLines"><input maxlength="20" type="text" id="surname" size="25" value = "<%= user.getSurname() %>"></td>

                        <td class="TableLines"><span class="ErrorMessage" id='surnameF'></span></td>
                    </tr>
                    <tr>
                        <td class="TableLines" align="right" valign="top">Имя: </td>
                        <td class="TableLines"><input maxlength="20" type="text"  id="name" size="25" value = "<%= user.getName() %>"> </td>
                        <td class="TableLines"><span class="ErrorMessage" id='nameF'></span></td>
                    </tr>
                    <tr>
                        <td class="TableLines" align="right" valign="top">Отчество: </td>
                        <td class="TableLines"><input type="text" id="patronicName" size="25" value = "<%= user.getPatronicName() %>"></td>
                    </tr>
                    <tr>
                        <td class="TableLines" align="right" valign="top">E-mail: </td>
                        <td class="TableLines"><input type="text" id="email" size="25" value = "<%= user.getEmail() %>"></td>
                        <td class="TableLines"><span class="ErrorMessage" id='emailF'></span></td>
                    </tr>

                    <tr class="pLink_tr">
                        <td class="TableLines" align="right" valign="top"></td>
                        <td><a href="#" class="pLink">Изменить пароль</a></td>
                    </tr>
                    <%--<tr class="TableLines pEdit" >--%>
                        <%--<td class="TableLines" align="right" valign="top">Введите старый пароль: </td>--%>
                        <%--<td class="TableLines"><input id="oldPass" class="newPasMenu" type="password" size="25"></td>--%>
                        <%--<td class="TableLines"><span class="ErrorMessage" id='oldPassF'></span></td>--%>
                        <%--&lt;%&ndash;<td class="TableLines"><span class="ErrorMessage" id='oldPasswordF'></span></td>&ndash;%&gt;--%>
                    <%--</tr>--%>

                    <tr class="TableLines pEdit" >
                        <td class="TableLines" align="right" valign="top">Введите новый пароль: </td>
                        <td class="TableLines"><input id="password" class="newPasMenu" type="password" size="25"></td>
                        <td class="TableLines">
                            <span class="ErrorMessage" id='passwordF'></span>
                            <span class="ErrorMessage" id='passwordF2'></span>
                            <span class="ErrorMessage" id='passwordF3'></span>
                        </td>
                    </tr>

                    <tr class="TableLines pEdit" >
                        <td class="TableLines" align="right" valign="top">Повторите пароль: </td>
                        <td class="TableLines"><input id="password2"  class="newPasMenu" type="password" size="25"></td>
                        <td class="TableLines">
                            <span class="ErrorMessage" id='passwordFF'></span>
                            <span class="ErrorMessage" id='passwordFF2'></span>
                            <span class="ErrorMessage" id='passwordFF3'></span>
                        </td>
                    </tr>

                    <tr class="pCancel_tr pEdit">
                        <td class="TableLines" align="right" valign="top"></td>
                        <td><a href="#" class="pLink cancelLink">Отменить изменение</a></td>
                    </tr>

                    <tr>
                        <td class="TableLines" align="right" valign="top">Дата рождения: </td>
                        <td class="TableLines"><input type="text" size="25" disabled value = "<%= user.getDateOfBirth() == null ? " " : format.format(user.getDateOfBirth()) %>"></td>
                    </tr>

                    <tr>
                        <td class="TableLines" align="right" valign="top">Телефон: </td>
                        <td class="TableLines"><input type="tel" id="phone" size="25" value = "<%= user.getTelephone() %>"></td>
                    </tr>

                    <tr>
                        <td class="TableLines" align="right" valign="top">Должность: </td>
                        <td class="TableLines"><input type="text" size="25" disabled value = "<%=  groupsString %>"></td>
                    </tr>

                    <tr>

                        <td colspan="2">
                            <button type="submit" class="iBut addButtons butPA" id="performPA"><i class="icon-hdd"></i>Сохранить</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>

    </div>

    <div id="footer"></div>

</div>

<script>

    $(document).ready(function(){
        $('.pEdit').hide();

        $('.pLink').click(function(e){
            e.preventDefault();
            $('.pEdit').toggle();
            $('.pLink_tr').toggle();
        });

        $('.cancelLink').click(function(){
            $('.newPasMenu').val('');
        });
    });

    $(document).ready(function($) {
        $("#PA").submit(function(event) {

            // Условия вывода ошибок при заполнении формы
            event.preventDefault();

            var password1 =  document.getElementById('password').value;
            var password2 =  document.getElementById('password2').value;
            var name = document.getElementById('name').value;
            var surname = document.getElementById('surname').value;
            var email = document.getElementById('email').value;
            var IsValid = true;  //Задаем переменную валидации формы

            //Очистка полей сообщений об ошибочном заполнении формы

            document.getElementById('surnameF').innerHTML = '';
            document.getElementById('nameF').innerHTML = '';
            document.getElementById('emailF').innerHTML = '';
            document.getElementById('passwordF2').innerHTML='';
            document.getElementById('passwordF').innerHTML='';
            document.getElementById('passwordF3').innerHTML='';
            document.getElementById('passwordFF2').innerHTML='';
            document.getElementById('passwordFF').innerHTML='';
            document.getElementById('passwordFF3').innerHTML='';
//            document.getElementById('oldPasswordF').innerHTML='';

            //Условия и вывод сообщений ошибки, при неккоректном заполнении формы


            if (surname == "") {
                document.getElementById('surnameF').innerHTML = '*Введите фамилию';
                IsValid = false;
            }

            if (name == "") {
                document.getElementById('nameF').innerHTML = '*Введите имя';
                IsValid = false;
            }

            dog=email.indexOf("@");
            point=email.indexOf(".");

            if (email == "") {
                document.getElementById('emailF').innerHTML = '*Введите Email';
                IsValid = false;
            }  else if (dog<1 || point <1) {
                document.getElementById('emailF').innerHTML = '*Email введен не верно';
                IsValid = false;
            }

//            if ($('#oldPass').val()== "") {
//                document.getElementById('oldPasswordF').innerHTML='*Введите старый пароль';
//                IsValid = false;
//            }

        if($('#pEdit').show) {
            if (password1 != password2) {
                document.getElementById('passwordF').innerHTML = '*Пароли не совпадают';
                IsValid = false;
            } else if (password1.length <= 7) {
                document.getElementById('passwordF3').innerHTML = '*Слишком короткий пароль';
                IsValid = false;
            }

            if (password1 != password2) {
                document.getElementById('passwordFF').innerHTML='*Пароли не совпадают';
                IsValid = false;
            } else if (password1.length <= 7) {
                document.getElementById('passwordFF3').innerHTML='*Слишком короткий пароль';
                IsValid = false;
            }

        }

            if(!IsValid){
                return;
            }

            $("#performPA").prop("disabled", false);
            editUserAjax();
        });
    });

            <%--$(document).ready(function(){--%>
                <%--$('#oldPass').change(function(){--%>

                    <%--var password =  $("#oldPass").val();--%>
                    <%--var IsValid = true;  //Задаем переменную валидации формы--%>
                    <%--document.getElementById('oldPassF').innerHTML='';--%>
                    <%----%>

                    <%--$.ajax({--%>
                        <%--type : "POST",--%>
                        <%--url : "${home}users/checkPassword",--%>
                        <%--data : { password: password },--%>
                        <%--dataType : 'json',--%>
                        <%--timeout : 100000,--%>
                        <%--success : function(data) {--%>
                            <%--console.log("SUCCESS: ", data);--%>
                            <%--if(data["code"] == "1") {--%>
<%--//                                alert("Пароль совпал");--%>
                                <%--document.getElementById('oldPassF').innerHTML='';--%>
                                <%--IsValid = false;--%>
                            <%--}--%>
                            <%--else{--%>
                                <%--document.getElementById('oldPassF').innerHTML='*Неверный пароль';--%>
                                <%--IsValid = false;--%>
                            <%--}--%>
                            <%--if(!IsValid){--%>
                                <%--return;--%>
                            <%--}--%>
                        <%--},--%>
                        <%--error : function(e) {--%>
                            <%--console.log("ERROR: ", e);--%>

                            <%--//editUserAjax();--%>

                        <%--}--%>

                    <%--});--%>
                <%--});--%>
            <%--});--%>

            function editUserAjax() {
                var data = {}
                data["name"] = $("#name").val();
                data["surname"] = $("#surname").val();
                data["patronicName"] = $("#patronicName").val();
                data["email"] = $("#email").val();
                data["password"] = $("#password").val();
                data["telephone"] = $("#phone").val();
//
//                if($('#pLink').onclick) {
//                    var password = $("#password").val();
//                }

                $.ajax({
                    type : "POST",
                    contentType : "application/json",
                    url : "${home}users/form/update",
                    data : JSON.stringify(data),
                    dataType : 'json',
                    timeout : 100000,
                    success : function(data) {
                        var parent = deleteElementById("PA");
                        console.log("SUCCESS: ", data);
                        if(data["code"] == "200") {
                            addTableStringToId(parent, 'Данные успешно сохранены');
//                            alert("Данные успешно сохранены");
                        }
                        else{
                            addTableStringToId(parent, "Что-то пошло не так");
//                            alert("Неудача");
                        }
                    },
                    error : function(e) {
                        console.log("ERROR: ", e);
                        //var parent = deleteElementById("PA");
                        //addTableStringToId(parent, "О, Боже! Что-то пошло не так");

                    }
                });
            }

            function addTableStringToId(parent, text)
            {
                var tableNode = document.createElement("table"); // <table align="center" border="0">
                tableNode.setAttribute("align","center");
                tableNode.setAttribute("border","0");
                tableNode.setAttribute("class","infMess");

                var trNode = document.createElement("tr");
                var tdNode = document.createElement("td");

                var textNode = document.createElement("h2");
                textNode.innerHTML = text;

                tableNode.appendChild(trNode);
                trNode.appendChild(tdNode);
                tdNode.appendChild(textNode);
                parent.appendChild(tableNode);
            }

            function deleteElementById(id) {
                var contentReg = document.getElementById(id);
                var parent = contentReg.parentNode;
                parent.removeChild(contentReg);
                return parent;
            }

    </script>


</body>
</html>
