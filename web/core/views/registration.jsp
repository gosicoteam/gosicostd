<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 30.10.16
  Time: 22:47
  To change this template use File | Settings | File Templates.
  регистарция
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <title>Регистрация</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <spring:url value="/resources/js/jquery-3.1.1.min.js" var="jqueryJs" />
    <script src="${jqueryJs}"></script>
</head>
<body>
<c:url var="home" value="/" scope="request" />

<div id="reg">
    <div id="header">
        <div id="hat-menu">
                    <form action="/" method="get">
                        <button type="submit" class="logoBut"></button>
                    </form>
        </div>
    </div>
    <div id="contentReg" align="center">
        <form id="registration" class="tables">

                        <table align="left" border="0">

                           <h1 id="RegHat" class="header1"><font face="Century Gothic" size=6 color="white">Регистрация</font></h1>

                            <tr>
                                <td class="TableLines" align="right" valign="top">Логин*</td>
                                <td class="TableLines"><input type="text" id="login" size="25"> <span class="ErrorMessage" id='loginf'></span>
                                    <span class="ErrorMessage" id='loginf2'></span></td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top">Фамилия*</td>
                                <td class="TableLines"><input maxlength="20" type="text" id="surname" size="25"> <span class="ErrorMessage" id='surnamef'></span></td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top">Имя*</td>
                                <td class="TableLines"><input maxlength="20" type="text" id="name" size="25"> <span class="ErrorMessage" id='namef'></span></td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top">Отчество</td>
                                <td class="TableLines"><input type="text" id="patronicName" size="25"> <span class="ErrorMessage" id='patronicNamef'></span></td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top">E-mail*</td>
                                <td class="TableLines"><input type="text" id="email" size="25"> <span class="ErrorMessage"  id='emailf'></span></td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top">Пароль*</td>
                                <td class="TableLines">
                                    <input type="password" id="password" size="25"> <span class="ErrorMessage" id='passwordf'></span>
                                    <span class="ErrorMessage" id='passwordf2'></span><span  class="ErrorMessage" id='passwordf3'></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top" >Повтор пароля*</td>
                                <td class="TableLines">
                                    <input type="password" id="password2" size="25"> <span class="ErrorMessage" id='passwordff'></span>
                                    <span class="ErrorMessage" id='passwordff2'></span><span class="ErrorMessage" id='passwordff3'></span>
                                </td>
                            </tr>



                            <tr>
                                <td class="TableLines" align="right" valign="top" >Пол*</td>
                                <td class="TableLines">
                                    <input type="radio" name="sex" class="radio" id="male" value="1" checked>
                                    <label for="male" >Мужской</label>

                                    <input type="radio" name="sex" class="radio" id="famale" value="0"/>
                                    <label for="famale">Женский</label>
                                </td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top">Дата Рождения*</td>
                                <td class="TableLines"><input type="date" id="date" name="birthday"/> <span class="ErrorMessage" id='datef'></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top">Телефон</td>
                                <td class="TableLines"><input type="tel" id="phone" size="25"></td>
                            </tr>

                            <tr>
                                <td class="TableLines" align="right" valign="top">Должность*</td>
                                <td class="TableLines">

                                    <select size="1" id="group">
                                        <option selected value="3">Студент
                                        <option value="4">Староста
                                        <option value="2">Преподаватель
                                    </select>
                                </td>
                            </tr>



                            <tr>
                                <td colspan="2">
                                    <button type="submit" name="reg" id="performRegistration"></button>
                                </td>
                            </tr>

                        </table>

                    </form>
    </div>
    <div id="footer"></div>
</div>

<script>
    jQuery(document).ready(function($) {
        $("#registration").submit(function(event) {

            // Условия вывода ошибок при заполнении формы
            event.preventDefault();

            var password1 =  document.getElementById('password').value;
            var password2 =  document.getElementById('password2').value;
            var login = document.getElementById('login').value;
            var name = document.getElementById('name').value;
            var surname = document.getElementById('surname').value;
            var email = document.getElementById('email').value;
            var date = document.getElementById('date').value;
            var IsValid = true;  //Задаем переменную валидации формы

            //Очистка полей сообщений об ошибочном заполнении формы

            document.getElementById('loginf').innerHTML = '';
            document.getElementById('loginf2').innerHTML = '';
            document.getElementById('surnamef').innerHTML = '';
            document.getElementById('namef').innerHTML = '';
            document.getElementById('emailf').innerHTML = '';
            document.getElementById('passwordf2').innerHTML='';
            document.getElementById('passwordf').innerHTML='';
            document.getElementById('passwordf3').innerHTML='';
            document.getElementById('passwordff2').innerHTML='';
            document.getElementById('passwordff').innerHTML='';
            document.getElementById('passwordff3').innerHTML='';
            document.getElementById('datef').innerHTML='';

            //Условия и вывод сообщений ошибки, при неккоректном заполнении формы

            if (login == "") {
                document.getElementById('loginf').innerHTML = '*Введите логин';
                IsValid = false;
            } else if(login.length <= 4) {
                document.getElementById('loginf2').innerHTML='*Логин слишком короткий';
                IsValid = false;
            }

            if (surname == "") {
                document.getElementById('surnamef').innerHTML = '*Введите фамилию';
                IsValid = false;
            }

            if (name == "") {
                document.getElementById('namef').innerHTML = '*Введите имя';
                IsValid = false;
            }

            dog=email.indexOf("@");
            point=email.indexOf(".");

            if (email == "") {
                document.getElementById('emailf').innerHTML = '*Введите Email';
                IsValid = false;
            }  else if (dog<1 || point <1) {
                document.getElementById('emailf').innerHTML = '*Email введен не верно';
                IsValid = false;
            }


            if (password1 == "") {
                document.getElementById('passwordf2').innerHTML='*Введите пароль';
                IsValid = false;
            }  else if (password1 != password2) {
                document.getElementById('passwordf').innerHTML='*Пароли не совпадают';
                IsValid = false;
            } else if (password1.length <= 7) {
                document.getElementById('passwordf3').innerHTML='*Слишком короткий пароль';
                IsValid = false;
            }

            if (password2 == "") {
                document.getElementById('passwordff2').innerHTML='*Введите пароль';
                IsValid = false;
            } else if (password1 != password2) {
                document.getElementById('passwordff').innerHTML='*Пароли не совпадают';
                IsValid = false;
            } else if (password1.length <= 7) {
                document.getElementById('passwordff3').innerHTML='*Слишком короткий пароль';
                IsValid = false;
            }

            if (date == "") {
                document.getElementById('datef').innerHTML = '*Укажите дату рождения';
                IsValid = false;
            }

            //

            if(!IsValid){
                return;
            }

            $("#performRegistration").prop("disabled", false);
            createNewUserAjax();
        });
    });


    function createNewUserAjax() {
        var data = {}
        data["name"] = $("#name").val();
        data["surname"] = $("#surname").val();
        data["patronicName"] = $("#patronicName").val();
        data["email"] = $("#email").val();
        data["password"] = $("#password").val();
        data["login"] = $("#login").val();
        data["dateOfBirth"] = $("#date").val();
        data["telephone"] = $("#phone").val();
        data["group"] = $("#group").val();

        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "${home}users/create",
            data : JSON.stringify(data),
            dataType : 'json',
            timeout : 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);
                var parent = deleteElementById("registration");
                if(data["code"] == "200") {
                    addTableStringToId(parent, 'Вы успешно зарегистрировались. Дождитесь активации аккаунта администратором.');
                }
                else{
                    addTableStringToId(parent, "О, Боже! Что-то пошло не так");
                }
            },
            error : function(e) {
                console.log("ERROR: ", e);
                var parent = deleteElementById("registration");
                addTableStringToId(parent, "О, Боже! Что-то пошло не так");

            },
            done : function(e) {
                $("#performRegistration").prop("disabled", true);
                console.log("DONE");
            }
        });
    }

    function addTableStringToId(parent, text)
    {
        var tableNode = document.createElement("table"); // <table align="center" border="0">
        tableNode.setAttribute("align","center");
        tableNode.setAttribute("border","0");
        tableNode.setAttribute("class","infMess");

        var trNode = document.createElement("tr");
        var tdNode = document.createElement("td");

        var textNode = document.createElement("h2");
//        textNode.setAttribute("size","16");
        textNode.innerHTML = text;

        tableNode.appendChild(trNode);
        trNode.appendChild(tdNode);
        tdNode.appendChild(textNode);
        parent.appendChild(tableNode);
    }

    function deleteElementById(id) {
        var contentReg = document.getElementById(id);
        var parent = contentReg.parentNode;
        parent.removeChild(contentReg);
        return parent;
    }

</script>

</body>
</html>
