<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 03.11.16
  Time: 1:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ошибка</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
</head>
<body>
<div id="Error">
    <div id="header">
        <div id="hat-menu">
            <form action="/mainPanel" method="get">
                <button type="submit" class="logoBut"></button>
            </form>
        </div>
    </div>
    <div class="infMess"><h2>О, Боже! Что-то пошло не так</h2></div>
</div>
</body>
</html>
