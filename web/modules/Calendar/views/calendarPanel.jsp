<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 02.11.16
  Time: 0:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Календарь</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel='stylesheet' href='/resources/modules/Calendar/js/fullcalendar.css' />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/easydropdown.css" />
    <script src='/resources/modules/Calendar/js/lib/jquery.min.js'></script>
    <script src='/resources/modules/Calendar/js/lib/moment.min.js'></script>
    <script src='/resources/modules/Calendar/js/fullcalendar.js'></script>
    <script src='/resources/modules/Calendar/js/locale/ru.js'></script>
    <script src="/resources/js/jquery.easydropdown.min.js"></script>
</head>
<body>

<div id="CalendarPanel">

    <div id="header">
        <div id="hat-menu">
            <form action="/mainPanel" method="get">
                <button type="submit" class="logoBut"></button>
            </form>
        </div>
    </div>

    <div id="left-menu">
        <div id="sortingNotes">
            <select size="1" id="typeNote" class="dropdown">
                <option selected value="0">Выберите тип
                <%--<option value="1" style="font-weight: bold;">Расписание--%>
                <option value="2" style="font-weight: bold;">Встреча
                <option value="3" style="font-weight: bold;">Праздник
                <option value="4" style="font-weight: bold;">Справочные материалы
                <option value="5" style="font-weight: bold;">Общее
            </select>
        </div>
        <div id="left-bar">
            <button type="submit" id="butcalendar"></button>
            <style>#butcalendar{
                background: url("/resources/modules/Calendar/icon.png") center / 100% 100%;
            }</style>
        </div>
    </div>

    <div class="content" id="content">
        <div id='calendar'></div>
    </div>

    <div id="footer"></div>

        <script>

            $("#typeNote").on('change', function(){
                var value = $(this).val();

                var str = window.location.search;
                str = replaceQueryParam('type', value, str);
                window.location = window.location.pathname + str;
            })


            $(document).ready(function() {

                var type = getParameterByName("type");
                type = type == null ? 0 : type;

                var element = document.getElementById('typeNote');
                element.value = type;

                $('#calendar').fullCalendar({

                    height: document.getElementById('content').clientHeight - 10,
                    timezone: false,
                    selectable: true,
                    editable: true,
                    navLinks: true,
                    eventLimit: true,

                    header: {
                        left: 'prev,next,today',
                        center: 'title',
                        right: 'month, agendaWeek, agendaDay'
                    },

                    eventResize: function(event)
                    {
                        updateEvent(event);
                    },

                    eventDrop: function(event)
                    {
                        updateEvent(event);
                    },

                    eventClick: function(event) {
                        if (event.idNote) {
                            window.open( window.location.protocol + '//' +  window.location.host +  "/getevent?id=" + event.idNote);
                            return false;
                        }
                    },

                    eventSources: [
                        {
                            url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                            type: 'POST',
                            data: {
                                priority: 1,
                                type: type
                            },
                            error: function() {
                                alert('there was an error while fetching events!');
                            },
                            color: 'red',   // a non-ajax option
                            textColor: 'black' // a non-ajax option

                        },

                        {
                            url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                            type: 'POST',
                            data: {
                                priority: 2,
                                type: type
                            },
                            error: function() {
                                alert('there was an error while fetching events!');
                            },
                            color: 'yellow',   // a non-ajax option
                            textColor: 'black' // a non-ajax option

                        },

                        {
                            url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                            type: 'POST',
                            data: {
                                priority: 3,
                                type: type
                            },
                            error: function() {
                                alert('there was an error while fetching events!');
                            },
                            color: 'green',   // a non-ajax option
                            textColor: 'black' // a non-ajax option

                        }
                    ],


                    color: 'yellow',   // an option!
                    textColor: 'black' // an option!
                })

            });

            function replaceQueryParam(param, newval, search) {
                var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
                var query = search.replace(regex, "$1").replace(/&$/, '');

                return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
            }

            function updateEvent(event) {

                var data = {};
                data["idNote"] = event.idNote;
                data["start"] = new Date(event.start);
                data["end"] = new Date(event.end);

                $.ajax({
                    type : "POST",
                    contentType : "application/json",
                    url : "/update/note",
                    data : JSON.stringify(data),
                    dataType : 'json',
                    timeout : 100000,
                    success : function(data) {
                        console.log("SUCCESS: ", data);
                    },
                    error : function(e) {
                        console.log("ERROR: ", e);
                    },
                    done : function(e) {

                    }
                });

            }

            function getParameterByName(name) {
                var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            }

        </script>
</body>
</html>
