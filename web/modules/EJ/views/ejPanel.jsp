<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Электронный журнал</title>
    <spring:url value="/resources/js/jquery-3.1.1.min.js" var="jqueryJs" />
    <script src="${jqueryJs}"></script>
    <script src="/resources/js/jquery.easydropdown.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/tablesStyle.css" />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/easydropdown.css" />
</head>
<body>
<div id="ejPanel">

<div id="header">
    <div id="hat-menu">
        <form action="/mainPanel" method="get">
            <button type="submit" class="logoBut"></button>
        </form>
    </div>
</div>

<div id="left-menu">
    <div id="left-bar">
        <button type="submit" id="butEJ"></button>
        <style>#butEJ{
            background: url("/resources/modules/EJ/icon.png") center / 100% 100%;
        }</style>
    </div>
</div>

<div id="content" align="center">
    <div style="width: 250px">
    <select id="selectGroups" class="dropdown">
        <option value = "0">Выберите группу</option>
        <c:forEach items="${groups}" var="group">
            <c:if test="${selectedGroup == group.idAcameicGroup}">
                <option selected value = "${group.idAcameicGroup}">${group.nameAGroup}</option>
            </c:if>
            <c:if test="${selectedGroup != group.idAcameicGroup}">
                <option value = "${group.idAcameicGroup}">${group.nameAGroup}</option>
            </c:if>
        </c:forEach>
    </select>
    </div>

    <form class="ej">
        <%--<table border="1" class="simple-little-table">--%>
            <%--<c:forEach items = "${dateList}" var = "date">--%>
                <%--<tr>--%>
                    <%--<td>--%>
                        <%--<c:out value="${date}"/>--%>
                    <%--</td>--%>
                <%--</tr>--%>
            <%--</c:forEach>--%>
        <%--</table>--%>

        <%--<table border="1" class="simple-little-table">--%>
        <%--<c:forEach items="${userList}" var = "user" >--%>
            <%--<tr>--%>
                <%--<td><c:out value="${user.userID}"/></td>--%>
                <%--<td><c:out value="${user.userName}"/></td>--%>
                <%--<c:forEach items="${user.values}" var = "value" >--%>
                    <%--&lt;%&ndash;<td><c:out value="${value.value}"/></td>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;<td><c:out value="${value.date}"/></td>&ndash;%&gt;--%>
                    <%--<td><button onclick="insideTableAction()">${value.value}</button></td>--%>
                <%--</c:forEach>--%>


            <%--</tr>--%>
        <%--</c:forEach>--%>
        <%--</table>--%>

        <table border="1" class="simple-little-table">
                <td style="vertical-align: top;">
                    <c:forEach items="${subjectList}" var = "subject" >
                        <h3>
                            <button type="button" onclick="updatePage(${subject.idSubject});">
                                <c:out value="${subject.nameSubject}"/>
                            </button>
                            </h3><br>
                    </c:forEach>
                </td>
                <td>

                    <table id="table"  class="little-table">
                        <thead>
                        <tr>
                            <th colspan="2">Дата:</th>
                            <c:forEach items = "${dateList}" var = "date">
                            <td><c:out value="${date}"/></td>
                            </c:forEach>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Фамилия, Имя</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${userList}" var = "user" >
                        <tr>
                            <td><c:out value="${user.number}"/></td>
                            <td><c:out value="${user.userName}"/></td>
                            <c:forEach items="${user.values}" var = "value" >
                            <td><input style="text-align: center" class="userinput" type="text"
                                       data-userid = "${user.userID}"
                                       data-username="${user.userName}"
                                       data-date="${value.date}"
                                       value="${value.value}"
                                       size="1" maxlength="1"

                            ></td>
                            </c:forEach>
                            <%--<td><span id="super" onClick="superPuper()">5</span></td>--%>

                        </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                </td>
            </tr>
        </table>
    </form>
</div>
<div id="footer"></div>

</div>

<script>

    $(document).ready(function(){

        $('.userinput').on('change', function() {
            insideTableAction(this)
        });

        $('#selectGroups').on('change', function(){
            var id = this.value;
            alert(id);

            $.ajax({
                type: "POST",
                url: "/apps/EJ/setGroup",
                data: { idAcademicGroup: id },
                timeout: 100000,
                success : function(data) {
                    console.log("SUCCESS: ", data);
                    window.location = window.location.protocol + '//' +  window.location.host +  "/apps/EJ";
                },
                error: function(data) {
                    console.log("ERROR: ", data);
                },
                done: function(data) {
                    console.log("DONE: ", data);
                }

            });

        });

    });

    function insideTableAction(tagthis){

        var data = {};

        var userID = $(tagthis).attr("data-userid");
        var userName = $(tagthis).attr("data-username");
        var date = $(tagthis).attr("data-date");
        var value = $(tagthis).val();

        data['userId'] = userID;
        data['username'] = userName;
        data['date'] = date;
        data['value'] = value;

        alert (JSON.stringify(data));

        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "/apps/EJ/saveMarks",
            data : JSON.stringify(data),
            dataType : 'json',
            timeout : 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);

            },
            error : function(e) {
                console.log("ERROR: ", e);

            },
        });
    };

    function updatePage(idSubject)
    {
        var str = window.location.search;
        str = replaceQueryParam('idSubject', idSubject, str);
        window.location = window.location.pathname + str;
    }

    function replaceQueryParam(param, newval, search) {
        var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
        var query = search.replace(regex, "$1").replace(/&$/, '');

        return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
    }
//    function superPuper() {
//        var newTag = document.createElement('input');
//        newTag.setAttribute('type', 'text');
//        newTag.setAttribute('size', '1');
//        newTag.setAttribute('value', '');
//        document.getElementById('super').innerHTML = '';
//        document.getElementById('super').appendChild(newTag);
//    }
</script>

</body>
</html>

