<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 08.11.16
  Time: 22:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Сообщения</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
</head>
<body>

<div id="MessagesPanel">

<div id="header">
    <div id="hat-menu">
        <form action="/mainPanel" method="get">
            <button type="submit" class="logoBut"></button>
        </form>
    </div>
</div>

<div id="left-menu">
    <div id="left-bar">
        <button type="submit" id="butmessages"></button>
        <style>#butmessages{
            background: url("/resources/modules/Messages/icon.png") center / 100% 100%;
        }</style>
    </div>
</div>

<div id="content"></div>

<div id="footer"></div>
</div>
</body>
</html>

