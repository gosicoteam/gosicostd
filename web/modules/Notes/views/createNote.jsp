<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 13.02.17
  Time: 0:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <spring:url value="/resources/js/jquery-3.1.1.min.js" var="jqueryJs" />
    <script src="${jqueryJs}"></script>
    <title>Создание заметки</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel='stylesheet' href='/resources/modules/Calendar/js/fullcalendar.css' />
</head>
<body>
<div id="CreateNote">
    <div id="header">
        <div id="hat-menu">
            <form action="/" method="get">
                <button type="submit" formaction="/apps/Notes" class="logoBut"></button>
            </form>
        </div>
    </div>
    <div id="contentCreateNote" class="tab" align="center">

        <form id="request" class="tables">

            <table align="center" border="0">

                <h1 class="header1"><font face="Century Gothic" size=6 color="white">Создание заметки</font></h1>

                <tr>
                    <td class="TableLines" align="right" valign="top">Тип заметки: </td>
                    <td class="TableLines">
                        <select name="month" size="1" id="typeNote">
                            <option value="1" style="font-weight: bold;">Расписание
                            <option value="2" style="font-weight: bold;">Встреча
                            <option value="3" style="font-weight: bold;">Праздник
                            <option value="4" style="font-weight: bold;">Справочные материалы
                            <option selected value="5" style="font-weight: bold;">Общее
                        </select></td>
                    <%--&#8226--%>
                </tr>
                <tr>
                    <td class="TableLines" align="right" valign="top">Приоритет: </td>
                    <td class="TableLines">
                        <select name="month" size="1" id="priority">
                            <option value="1" style="color: #d00001; font-weight: bold;">Срочная
                            <option value="2" style="color: #eec400; font-weight: bold;">Важная
                            <option selected value="3" style="color: #14a800; font-weight: bold;">Не срочная
                        </select></td>
                </tr>


                <tr>
                    <td class="TableLines" align="right" valign="top">Дата начала: </td>
                    <td class="TableLines"><input type="date" id="dateNote">  <span class="ErrorMessage" id='dateStartF'></span></td>
                    <%--<td class="TableLines"><span class="ErrorMessage" id='dateStartF'></span></td>--%>
                </tr>

                <tr>
                    <td class="TableLines" align="right" valign="top">Время начала: </td>
                    <td class="TableLines"> <input type="time" id="timeStart">  <span class="ErrorMessage" id='timeStartF'></span></td>
                    <%--<td class="TableLines"><span class="ErrorMessage" id='timeStartF'></span></td>--%>
                </tr>

                <tr id="dateEndTR">
                    <td class="TableLines" align="right" valign="top">Дата окончания: </td>
                    <td class="TableLines"> <input type="date" id="dateEnd"></td>
                </tr>

                <tr>
                    <td class="TableLines" align="right" valign="top">Время окончания: </td>
                    <td class="TableLines"> <input type="time" id="timeEnd"></td>
                </tr>


                <tr>
                    <td class="TableLines" align="right" valign="top">Доступ: </td>
                    <td class="TableLines">
                        <select name="" size="1" id="accessNote">
                            <option value="3">Общий
                            <option value="2">Групповой
                            <option selected  value="1">Личный
                        </select></td>
                </tr>

                <tr id="addDis" style="display:none">
                    <td class="TableLines" align="right" valign="top">Выберите предмет: </td>
                    <td class="TableLines">
                        <select  size="1" id="subjects">
                            <option selected value="0">Выберите предмет</option>
                        <c:forEach items="${subjects}" var="subject">
                            <option value="${subject.idSubject}">${subject.nameSubject}</option>
                        </c:forEach>
                        </select>
                    </td>
                </tr>

                <tr  id="themeNoteTR">
                    <td class="TableLines" align="right" valign="top">Тема заметки: </td>
                    <td class="TableLines"><input type="text" size="25" id="themeNote">  <span class="ErrorMessage" id='themeNoteF'></span></td>
                    <%--<td class="TableLines"><span class="ErrorMessage" id='themeNoteF'></span></td>--%>
                </tr>

                <tr>
                    <td class="TableLines" align="right" valign="top">Текст заметки: </td>
                    <td class="TableLines"><textarea cols="50" rows="7" wrap="virtual" id="textNote"></textarea></td>
                    <td class="TableLines"><span class="ErrorMessage" id='textNoteF'></span></td>
                </tr>


                <tr>
                    <td colspan="2">
                        <button type="submit" id="sendRequest" class="iBut"><i class="icon-edit"></i>Создать заметку</button>
                        <input type="file"  class="iBut" id="addMaterials" style="display:none">
                    </td>
                </tr>




            </table>
        </form>

    </div>
    <div id="footer"></div>
</div>

</body>

<script>
    jQuery(document).ready(function(){
        $('#request').submit(function (event) {
            event.preventDefault();

            $('#sendRequest').prop("disabled", false);
            createNewNoteAjax();
        });
    });


    $(document).ready(function() {

        $("#typeNote").on('change', function () {             //При выборе справочные материалы, показываем кнопку добавления материала
            if ($(this).val() == 4) {
                $("#addMaterials").show();
            } else {
                $("#addMaterials").hide();
            }
        })

        $("#typeNote").on('change', function () {              //При выборе расписания, показываем выбор предмета
            if ($(this).val() == 1) {
                $("#addDis").show();
            } else {
                $("#addDis").hide();
            }
        })

        $("#typeNote").on('change', function () {           //Скрываем поля тема заметки и дата окончания

            if ($(this).val() == 1) {
                $("#themeNoteTR").hide();
                $("#dateEndTR").hide();

            } else {
                $("#themeNoteTR").show();
                $("#dateEndTR").show();
            }
        })

    })

    function createNewNoteAjax() {

//        var dateStartNote =  document.getElementById('dateNote').value;
//        var timeStartNote =  document.getElementById('timeStart').value;
//        var dateEndNote =  document.getElementById('dateEnd').value;
//        var timeEndNote =  document.getElementById('timeEnd').value;
//
//        var dateTimeStart = parseInt(dateStartNote) + " " + parseInt(timeStartNote);
//        var dateTimeEnd = parseInt(dateEndNote) + " " + parseInt(timeEndNote);

        var data = {};
//        data["dateNote"] = dateTimeStart;
//        data["dateEnd"] = dateTimeEnd;
        if($("#timeStart").val() != '')
        {
            data["dateNote"] = $("#dateNote").val() + 'T'  + $("#timeStart").val() + ':00.000Z';
        }
        if($("#timeStart").val() == '' || $("#dateNote").val() == null)
        {
            data["dateNote"] = $("#dateNote").val();
        }

        if($("#timeEnd").val() != '')
        {
            data["dateEnd"] = $("#dateEnd").val() + 'T' + $("#timeEnd").val() + ':00.000Z';
        }
        if($("#timeEnd").val() == '' || $("#timeEnd").val() == null)
        {
            data["dateEnd"] = $("#dateEnd").val();
        }

        data["themeNote"] = $("#themeNote").val();
        data["textNote"] = $("#textNote").val();
        data["priority"] = $("#priority").val();
        data["typeNote"] = $("#typeNote").val();
        data["accessNote"] = $("#accessNote").val();

        if($("#subjects").val() != 0){
            data["idSubject"] = $("#subjects").val();
        }

        var IsValid = true;  //Задаем переменную валидации формы

        //Очистка полей сообщений об ошибочном заполнении формы

        document.getElementById('dateStartF').innerHTML = '';
        document.getElementById('timeStartF').innerHTML = '';
        document.getElementById('themeNoteF').innerHTML = '';
        document.getElementById('themeNoteF').innerHTML = '';
        document.getElementById('textNoteF').innerHTML = '';
        //Условия и вывод сообщений ошибки, при неккоректном заполнении формы

        if($('#dateNote').val() == 0)
        {
            document.getElementById('dateStartF').innerHTML = '*Выберите дату';
            IsValid = false;
//                alert('Укажите кафедру');
//                return;
        }


        if ($('#timeStart').val() == "") {
            document.getElementById('timeStartF').innerHTML = '*Выберите время';
            IsValid = false;
        }

        if($('#typeNote').val() != 1){
            if ($('#themeNote').val() == "") {
                document.getElementById('themeNoteF').innerHTML = '*Введите тему';
                IsValid = false;
            }
        }

        if ($('#textNote').val() == "") {
            document.getElementById('textNoteF').innerHTML = '*Введите текст';
            IsValid = false;
        }



        if(!IsValid){
            return;
        }

        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "/apps/Notes/create",
            data : JSON.stringify(data),
            dataType : 'json',
            timeout : 100000,
            success : function(data) {
                console.log("SUCCESS: ", data);
                document.getElementById('dateNote').value = "";
                document.getElementById('dateEnd').value = "";
                document.getElementById('themeNote').value = "";
                document.getElementById('textNote').value = "";
                var parent = deleteElementById("request");
                addTableStringToId(parent, 'Заметка была успешно создана.');
//                alert('Заметка была успешно создана.');
            },
            error : function(e) {
                console.log("ERROR: ", e);

            },
            done : function(e) {
                console.log("DONE");
                $('#sendRequest').prop("disabled", false);
            }
        });
    }

    function addTableStringToId(parent, text)
    {
        var tableNode = document.createElement("table"); // <table align="center" border="0">
        tableNode.setAttribute("align","center");
        tableNode.setAttribute("border","0");
        tableNode.setAttribute("class","infMess");

        var trNode = document.createElement("tr");
        var tdNode = document.createElement("td");

        var textNode = document.createElement("h2");
//        textNode.setAttribute("size","16");
        textNode.innerHTML = text;

        tableNode.appendChild(trNode);
        trNode.appendChild(tdNode);
        tdNode.appendChild(textNode);
        parent.appendChild(tableNode);
    }

    function deleteElementById(id) {
        var contentReg = document.getElementById(id);
        var parent = contentReg.parentNode;
        parent.removeChild(contentReg);
        return parent;
    }




//
//    var disSelect = document.getElementById("addDis");
//    var disText = disSelect.options[disSelect.selectedIndex].text;
//    var themeDis = disText;
//    document.getElementById("themeNote").value = themeDis;


</script>

</html>
