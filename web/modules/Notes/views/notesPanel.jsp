<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 08.11.16
  Time: 23:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src='/resources/modules/Calendar/js/lib/jquery.min.js'></script>
<script src='/resources/modules/Calendar/js/lib/moment.min.js'></script>
<script src='/resources/modules/Calendar/js/fullcalendar.js'></script>
<script src='/resources/modules/Calendar/js/locale/ru.js'></script>
<html>
<head>
    <title>Заметки</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel='stylesheet' href='/resources/modules/Calendar/js/fullcalendar.css' />
</head>
<body>
<div id="notesPanel">

<div id="header">
    <div id="hat-menu">
        <form action="/mainPanel" method="get">
            <button type="submit" class="logoBut"></button>
        </form>
    </div>
</div>

<div id="left-menu">
    <div id="left-bar">
        <button type="submit" id="butnotes"></button>
        <style>#butnotes{
            background: url("/resources/modules/Notes/icon.png") center / 100% 100%;
        }</style>
    </div>
</div>

<div class="content" id="content">

    <div id='calendar'></div>

    <form action="/apps/Notes/create">
        <button type="submit"  class="iBut" id="createNoteBut"><i class="icon-edit"></i>Создать заметку</button>
    </form>
</div>

<div id="footer"></div>

</div>
</body>

<script>

    $(document).ready(function() {
        $('#calendar').fullCalendar({

            height: document.getElementById('content').clientHeight - 10,

            defaultView: 'listWeek',

            header: {
                left: 'prev,next,today',
                center: 'title',
                right: ''
            },

            eventClick: function(event) {
                if (event.idNote) {
                    window.location.replace( window.location.protocol + '//' +  window.location.host +  "/getevent?id=" + event.idNote);
                    return false;
                }
            },

            eventSources: [

                {
                    url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                    type: 'POST',
                    data: {
                        priority: 1,
                        type: 0
                    },
                    error: function() {
                        alert('there was an error while fetching events!');
                    },
                    color: 'red',   // a non-ajax option
                    textColor: 'black' // a non-ajax option
                },

                {
                    url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                    type: 'POST',
                    data: {
                        priority: 2,
                        type: 0
                    },
                    error: function() {
                        alert('there was an error while fetching events!');
                    },
                    color: 'yellow',   // a non-ajax option
                    textColor: 'black' // a non-ajax option
                },

                {
                    url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                    type: 'POST',
                    data: {
                        priority: 3,
                        type: 0
                    },
                    error: function() {
                        alert('there was an error while fetching events!');
                    },
                    color: 'green',   // a non-ajax option
                    textColor: 'black' // a non-ajax option
                }
            ],


            color: 'yellow',   // an option!
            textColor: 'black' // an option!
        })

    });


</script>
</html>
