<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 15.02.17
  Time: 22:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="core.models.NoteModel"%>
<html>
<head>
    <title>Просмотр заметки</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />

    <spring:url value="/resources/js/jquery-3.1.1.min.js" var="jqueryJs" />
    <script src="${jqueryJs}"></script>

</head>
<body>

<div id="CreateNote">
    <div id="header">
        <div id="hat-menu">
            <form action="/" method="get">
                <button type="submit" formaction="/apps/Notes" class="logoBut"></button>
            </form>
        </div>
    </div>

    <%
        NoteModel note = (NoteModel) session.getAttribute("note");
        String theme = note.getThemeNote();
        String text = note.getTextNote();
    %>

    <div class="contentShowNote" align="center">

        <form id="showNote" class="tables">

            <input type="hidden" id="id" name="id" value="<% out.print(note.getIdNote()); %>" />

            <table align="center" border="0">

                <h2 class="header1"><font face="Century Gothic" size=5 color="white">Просмотр заметки</font></h2>

                <tr>
                    <td class="TableLines" align="right" valign="top">Тема заметки: </td>
                    <td class="TableLines"><input type="text" class="theme" name="theme" id="surname" size="25" value = "<% out.println(theme); %>"  style="font-size: 120%"></td>
                </tr>
                <tr>
                    <td class="TableLines" align="right" valign="top">Текст заметки: </td>
                    <td class="TableLines"><textarea id="text" name="text" cols="50" rows="7" wrap="virtual" style="font-size: 170%"><% out.println(text); %></textarea></td>
                </tr>

                <tr>

                    <td colspan="2">
                        <button id="delNote" type="submit" class="iBut addButtons"><i class="icon-remove"></i>Удалить заметку</button>
                        <button id="editNote" type="submit" class="iBut addButtons"><i class="icon-hdd"></i>Изменить</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    </div>


<script>
    $("#editNote").click(function(event)
    {
        event.preventDefault();

        var id = $('#id').val();
        var theme = $('.theme').val();
        var text = $('#text').val();

        $.ajax({
            type: "POST",
            url: "/update/note/edit",
            data: { id: id, theme: theme, text: text },
            timeout: 100000,
            success : function(data) {
                var parent = deleteElementById("showNote");
                addTableStringToId(parent, 'Заметка успешно изменена.');
                console.log("SUCCESS: ", data);
            },
            error: function(data) {
                addTableStringToId(parent, 'Что-то пошло не так.');
                console.log("ERROR: ", data);
            },
            done: function(data) {
                console.log("DONE: ", data);
            }

        });

    });

    $("#delNote").click(function(event)
    {
        event.preventDefault();

        var id = $('#id').val();

        $.ajax({
            type: "POST",
            url: "/note/delete",
            data: { idNote: id},
            timeout: 100000,
            success : function(data) {
                var parent = deleteElementById("showNote");
                addTableStringToId(parent, 'Заметка успешно удалена.');
                console.log("SUCCESS: ", data);

            },
            error: function(data) {
                addTableStringToId(parent, 'Что-то пошло не так');
                console.log("ERROR: ", data);
            },
            done: function(data) {
                console.log("DONE: ", data);
            }

        });

    });


    function addTableStringToId(parent, text)
    {
        var tableNode = document.createElement("table"); // <table align="center" border="0">
        tableNode.setAttribute("align","center");
        tableNode.setAttribute("border","0");
        tableNode.setAttribute("class","infMess");

        var trNode = document.createElement("tr");
        var tdNode = document.createElement("td");

        var textNode = document.createElement("h2");
//        textNode.setAttribute("size","16");
        textNode.innerHTML = text;

        tableNode.appendChild(trNode);
        trNode.appendChild(tdNode);
        tdNode.appendChild(textNode);
        parent.appendChild(tableNode);
    }

    function deleteElementById(id) {
        var contentReg = document.getElementById(id);
        var parent = contentReg.parentNode;
        parent.removeChild(contentReg);
        return parent;
    }
</script>

</body>
</html>
