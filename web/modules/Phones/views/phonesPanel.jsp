<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 08.11.16
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Телефонный справочник</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/tablesStyle.css" />
</head>
<body>
<div id="PhonesPanel">

<div id="header">
    <div id="hat-menu">
        <form action="/mainPanel" method="get">
            <button type="submit" class="logoBut"></button>
        </form>
    </div>
</div>

<div id="left-menu">
    <div id="left-bar">
        <button type="submit" id="butphones"></button>
        <style>#butphones{
            background: url("/resources/modules/Phones/icon.png") center / 100% 100%;
        }</style>
    </div>
</div>

<div id="content" align="center">
    <form class="phones">
        <%--<table align="center" class="simple-little-table">--%>
                <%--<tr>--%>
                    <%--<td rowspan="2">Моя группа<br>Преподаватели</td>--%>
                    <%--<th>ФИО</th>--%>
                    <%--<th>Контактный телефон</th>--%>
                    <%--<th>Электронная почта</th>--%>
                <%--</tr>--%>
            <%--<tbody class="little-table">--%>
                <%--<tr>--%>
                    <%--<td></td>--%>
                    <%--<td></td>--%>
                    <%--<td></td>--%>
                <%--</tr>--%>
            <%--</tbody>--%>
        <%--</table>--%>
            <table border="1" class="simple-little-table">
                <tr>
                    <td><h3><a href="" class="">Моя группа</a></h3><br><h3><a href="" class="">Преподаватели</a></h3></td>
                    <td>

                        <table id="table"  class="little-table">
                            <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>Контактный телефон</th>
                                <th>Электронная почта</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>155555555555515</td>
                                <td>1251515</td>
                                <td>111111111111111111111111111111111111111</td>
                            </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>
    </form>
</div>

<div id="footer"></div>

</div>
</body>
</html>
