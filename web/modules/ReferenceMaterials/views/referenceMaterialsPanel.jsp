<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 08.11.16
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Справочные материалы</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/tablesStyle.css" />
</head>
<body>
<div id="referenceMaterialsPanel"></div>

<div id="header">
    <div id="hat-menu">
        <form action="/mainPanel" method="get">
            <button type="submit" class="logoBut"></button>
        </form>
    </div>
</div>

<div id="left-menu">
    <div id="left-bar">
        <button type="submit" id="butreferenceMat"></button>
        <style>#butreferenceMat{
            background: url("/resources/modules/ReferenceMaterials/icon.png") center / 100% 100%;
        }</style>
    </div>
</div>

<div id="content" align="center">
    <form class="ej">
        <table border="1" class="simple-little-table">
            <tr>
                <td style="vertical-align: top;"><h3><a href="" class="">Предмет 1</a></h3><br><h3><a href="" class="">Предмет 2</a></h3></td>
                <td>

                    <table id="table"  class="little-table">
                        <thead>
                        <tr>
                            <th>Дата добавления</th>
                            <th>Отправитель</th>
                            <th>Комментарий</th>
                            <th>Справочный материал</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>17.02.2017</td>
                            <td>Иванов И.И.</td>
                            <td>TextArea</td>
                            <td>Прикрепленный файл <a href="">Кнопка скачивания</a></td>
                        </tr>
                        <tr>
                            <td>11.02.2017</td>
                            <td>Сидоров С.С.</td>
                            <td>TextArea</td>
                            <td>Прикрепленный файл <a href="">Кнопка скачивания</a></td>
                        </tr>
                        <tr>
                            <td>15.05.2017</td>
                            <td>Петров П.П.</td>
                            <td>TextArea</td>
                            <td>Прикрепленный файл <a href="">Кнопка скачивания</a></td>
                        </tr>
                        <tr>
                        </tbody>
                    </table>

                </td>
            </tr>
        </table>
    </form>
</div>

<div id="footer"></div>

</div>
</body>
</html>
