<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 08.11.16
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Расписание занятий</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel='stylesheet' href='/resources/modules/Calendar/js/fullcalendar.css' />
    <script src='/resources/modules/Calendar/js/lib/jquery.min.js'></script>
    <script src='/resources/modules/Calendar/js/lib/moment.min.js'></script>
    <script src='/resources/modules/Calendar/js/fullcalendar.js'></script>
    <script src='/resources/modules/Calendar/js/locale/ru.js'></script>
</head>
<body>
<div id="schedulePanel">

<div id="header">
    <div id="hat-menu">
        <form action="/mainPanel" method="get">
            <button type="submit" class="logoBut"></button>
        </form>
    </div>
</div>

<div id="left-menu">
    <div id="left-bar">
        <button type="submit" id="butschedule"></button>
        <style>#butschedule{
            background: url("/resources/modules/Schedule/icon.png") center / 100% 100%;
        }</style>
    </div>
</div>

    <div class="content" id="content">
        <div id='calendar'></div>
    </div>

    <div id="footer"></div>

    <script>
        $(document).ready(function() {
            $('#calendar').fullCalendar({

                height: document.getElementById('content').clientHeight - 10,
                timezone: false,
                selectable: true,
                editable: true,
                navLinks: true,
                eventLimit: true,

                defaultView: 'agendaWeek',

                header: {
                    left: 'prev,next,today',
                    center: 'title',
                    right: 'agendaWeek'
                },

                minTime: "08:00:00",
                maxTime: "19:00:00",
                slotLabelFormat: "H:mm",
                allDaySlot: false,
                slotDuration:"00:05:00",

//                businessHours: {
//
//                    dow: [ 1, 2, 3, 4, 5 ],
//
//                    start: '8:00',
//                    end: '19:00',
//                },


                eventResize: function(event)
                {
                    updateEvent(event);
                },

                eventDrop: function(event)
                {
                    updateEvent(event);
                },

                eventClick: function(event) {
                    if (event.idNote) {
                        window.open( window.location.protocol + '//' +  window.location.host +  "/getevent?id=" + event.idNote);
                        return false;
                    }
                },

                eventSources: [
                    {
                        url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                        type: 'POST',
                        data: {
                            priority: 1,
                            type: 1
                        },
                        error: function() {
                            alert('there was an error while fetching events!');
                        },
                        color: 'red',   // a non-ajax option
                        textColor: 'black' // a non-ajax option

                    },

                    {
                        url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                        type: 'POST',
                        data: {
                            priority: 2,
                            type: 1
                        },
                        error: function() {
                            alert('there was an error while fetching events!');
                        },
                        color: 'yellow',   // a non-ajax option
                        textColor: 'black' // a non-ajax option

                    },

                    {
                        url: window.location.protocol + '//' +  window.location.host + '/apps/Calendar/event',
                        type: 'POST',
                        data: {
                            priority: 3,
                            type: 1
                        },
                        error: function() {
                            alert('there was an error while fetching events!');
                        },
                        color: 'green',   // a non-ajax option
                        textColor: 'black' // a non-ajax option

                    }
                ],


                color: 'yellow',   // an option!
                textColor: 'black' // an option!
            })

        });

        function updateEvent(event) {

            var data = {};
            data["idNote"] = event.idNote;
            data["start"] = new Date(event.start);
            data["end"] = new Date(event.end);

            $.ajax({
                type : "POST",
                contentType : "application/json",
                url : "/update/note",
                data : JSON.stringify(data),
                dataType : 'json',
                timeout : 100000,
                success : function(data) {
                    console.log("SUCCESS: ", data);
                },
                error : function(e) {
                    console.log("ERROR: ", e);
                },
                done : function(e) {

                }
            });

        }


    </script>

</div>
</body>
</html>
