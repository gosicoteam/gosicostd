<%--
  Created by IntelliJ IDEA.
  User: homax
  Date: 08.11.16
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Поиск</title>
    <link rel="stylesheet" type="text/css" href="/resources/CSS/Project.css" />
    <link rel="stylesheet" type="text/css" href="/resources/CSS/tablesStyle.css" />
</head>
<body>
<div id="searchPanel">


<div id="header">
    <div id="hat-menu">
        <form action="/mainPanel" method="get">
            <button type="submit" class="logoBut"></button>
        </form>
    </div>
</div>

<div id="left-menu">
    <div id="left-bar">
        <button type="submit" id="butsearch"></button>
        <style>#butsearch{
            background: url("/resources/modules/Search/icon.png") center / 100% 100%;
        }</style>
    </div>
</div>

<div id="content" class="content" align="center">
    <table border="1" class="simple-little-table" >
        <tr>
            <td height="200">Поиск<br> по заметкам</td>
            <td>
                <table id="table"  class="little-table">
                        <form  action="/apps/Search" method="get">
                            <input type="text" size="25" name="searchParam" id="searchNotes" style="padding: 2px">
                            <button type="submit" class="iBut" id="searchNotesBut"><i class="icon-search"></i>Поиск</button>
                        </form>
                    <thead>
                    <tr>
                        <th>Тип заметки</th>
                        <th>Тема заметки</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items = "${list}" var = "value">
                    <tr>
                        <td>
                            <c:out value="${value.typeNote == 1 ? 'Занятие' :
                        value.typeNote == 2 ? 'Встреча' :
                        value.typeNote == 3 ? 'Праздник' :
                        value.typeNote == 4 ? 'Справочный материал' : 'Общее'}"/>
                        </td>
                        <td>
                            <a href="" onclick="window.open( window.location.protocol + '//' +  window.location.host +  '/getevent?id=' + ${value.idNote});"><c:out value="${value.themeNote}"/></a>
                        </td>
                    </tr>
                    </c:forEach>

                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <%--<br/>--%>
    <%--<c:forEach items = "${list}" var = "value">--%>
        <%--<c:out value="${value.typeNote == 1 ? 'Занятие' :--%>
                        <%--value.typeNote == 2 ? 'Встерача' :--%>
                        <%--value.typeNote == 3 ? 'Праздник' :--%>
                        <%--value.typeNote == 4 ? 'Справочний материал' : 'Общее'}"/> <c:out value="${value.themeNote}"/>--%>
        <%--<br/>--%>
    <%--</c:forEach>--%>

</div>

<div id="footer"></div>

</div>
</body>
</html>
